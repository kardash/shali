<?php
    // Settings of images on the site
    return array(
        // Watermark path
        'watermark' => 'pic/logo.png',
        // Image types
        'types' => array(
            'jpg', 'jpeg', 'png', 'gif',
        ),
        // Slider images
        'slider' => array(
            array(
                'path' => 'small',
                'width' => 200,
                'height' => 70,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 1860,
                'height' => 780,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
		
		'reviews' => array(
            array(
                'path' => 'small',
                'width' => 400,
                'height' => 290,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),


        // Products images
        'catalog' => array(
            array(
                'path' => 'small',
                'width' => 34,
                'height' => 50,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'medium',
                'width' => 210,
                'height' => 320,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 680,
                'height' => 715,
                'resize' => 1,
                'crop' => 0,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        'gallery' => array(
            array(
                'path' => '',
                'width' => 200,
                'height' => 200,
                'resize' => 1,
                'crop' => 1,
            ),
        ),
        'gallery_images' => array(
            array(
                'path' => 'small',
                'width' => 355,
                'height' => 355,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
    );