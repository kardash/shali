<?php
    
    return array(
        'frontend' => array(
            'Ajax', 'Index', 'Articles', 'News', 'Cart', 'User', 'Catalog', 'Unsubscribe',
            'Search', 'Sitemap', 'Contact', 'Content',
        ),
        'backend' => array(
            'Ajax', 'Index', 'Catalog', 'Config', 'Contacts', 'Content','Address', 'Index', 'Log', 'Visitors', 'Blog', 'Blacklist',
            'MailTemplates', 'Menu', 'Orders', 'Seo', 'Subscribe', 'User', 'Multimedia', 'Statistic', 'Reviews', 'Crop'
        ),
    );