<?php

    return array(
        'wezom/address/index' => 'address/address/index',
        'wezom/address/index/page/<page:[0-9]*>' => 'address/address/index',
        'wezom/address/edit/<id:[0-9]*>' => 'address/address/edit',
        'wezom/address/delete/<id:[0-9]*>' => 'address/address/delete',
        'wezom/address/add' => 'address/address/add',
    );
