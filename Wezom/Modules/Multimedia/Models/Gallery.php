<?php
    namespace Wezom\Modules\Multimedia\Models;

    use Core\Arr;
    use Core\Message;

    class Gallery extends \Core\Common {

        public static $table = 'gallery';
        public static $image = 'gallery';

        public static $rules = array(
            'name' => array(
                array(
                    'error' => 'Название фотоальбома не может быть пустым!',
                    'key' => 'not_empty',
                ),
            ),
        );

    }