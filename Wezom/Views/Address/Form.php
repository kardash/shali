<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(array('name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right')); ?>
    </div>
    <div class="col-md-12">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo \Forms\Builder::bool($obj->status); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'name' => 'FORM[city]',
                            'value' => $obj->city,
                            'class' => 'valid',
                        ), 'Город'); ?>
                    </div>
					<div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'name' => 'FORM[street]',
                            'value' => $obj->street,
                            'class' => 'valid',
                        ), 'Улица'); ?>
                    </div>
					<div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'name' => 'FORM[phone]',
                            'value' => $obj->phone,
                            'class' => 'valid',
                        ), 'Телефон'); ?>
                    </div>
                   
				   <?php $data = json_decode($obj->map, TRUE); ?>
					<div class="col-md-12">
						<div class="widget box">
							<div class="widgetHeader">
								<div class="widgetTitle">
									<i class="fa-reorder"></i>
									Настройка карты
								</div>
							</div>
							<div class="widgetContent">
								<div class="mapWrap" style="height: 450px;">
									<input type="text" id="address" class="form-control ui-autocomplete-input" placeholder="Начните писать адрес..." autocomplete="off"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
									<div style="width: 100%; height: calc(100% - 30px); position: relative; background-color: rgb(229, 227, 223); overflow: hidden;" id="gmap"></div>
								</div>
								<input type="hidden" value="<?php echo \Core\Arr::get($data, 'latitude'); ?>" name="latitude">
								<input type="hidden" value="<?php echo \Core\Arr::get($data, 'longitude'); ?>" name="longitude">
							</div>
						</div>
							<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAyqZU3EGbB02sCB-O0JXmYlqcnYlEvAXk" type="text/javascript"></script>
							<script type="text/javascript">
								$('#address').val('');
								var map = false;

								var geocoder = false;

								<?php if(!$data['longitude'] || !$data['latitude']): ?>
									var myLatlng = new google.maps.LatLng(48.93982088660896, 31.46484375);
									var zoom = 6;
									var marker = false;
								<?php else: ?>
									var myLatlng = new google.maps.LatLng(parseFloat('<?php echo $data['latitude']; ?>'), parseFloat('<?php echo $data['longitude']; ?>'));
									var zoom = 14;
									var marker = true;
								<?php endif; ?>

								function initialize() {
									var mapOptions = {
										center: myLatlng,
										zoom: zoom,
										mapTypeId: google.maps.MapTypeId.ROADMAP
									};
									map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
									geocoder = new google.maps.Geocoder();
								}

								function bind_location(location) {
									$('input[name="latitude"]').val( location.lat() );
									$('input[name="longitude"]').val( location.lng() );
								}

								function placeMarker(location) {
									if (marker) {
										marker.setMap(null);
									}
									bind_location(location);
									pointt = new google.maps.Point(18, 54);
									marker = new google.maps.Marker({
										position: location,
										map: map,
										draggable: true
									});
									google.maps.event.addListener(marker, 'dragend', function(cc){
										bind_location(cc.latLng);
									});
								}

								$(document).ready(function(){
									initialize();
									google.maps.event.addListener(map, 'click', function(cc){
										placeMarker(cc.latLng);
									});

									if(marker) {
										pointt = new google.maps.Point(18, 54);
										marker = new google.maps.Marker({
											position: myLatlng,
											map: map,
											draggable: true
										});
										google.maps.event.addListener(marker, 'dragend', function(cc){
											bind_location(cc.latLng);
										});
									}

									$("#address").autocomplete({
										//Определяем значение для адреса при геокодировании
										source: function(request, response) {
											geocoder.geocode( {'address': request.term}, function(results, status) {
												response($.map(results, function(item) {
													return {
														label:  item.formatted_address,
														value: item.formatted_address,
														latitude: item.geometry.location.lat(),
														longitude: item.geometry.location.lng()
													}
												}));
											})
										},
										select: function(event, ui) {
											$('input[name="latitude"]').val( ui.item.latitude );
											$('input[name="longitude"]').val( ui.item.longitude );
											var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
											map.setCenter(location);
											map.setZoom(14);
											placeMarker(location);
										}
									});
								});
							</script>
							   
					</div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>