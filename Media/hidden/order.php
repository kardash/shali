<div class="mfiModal mfiModal--form mfiModal--styled">
	<button title="Закрыть (ESC)" type="button" class="mfp-close">&times;</button>
	<div class="pageTitle">Заказать шаль</div>
	<div class="wTxt">
		<p>Наш менеджер свяжется с вами в течении 5 минут</p>
	</div>
	<div class="wForm wFormDef" data-form="true">
		<div class="wFormRowDouble _clear-after">
			<div class="wFormRow">
				<small>Ваше имя<span class="_color-danger">*</span></small>
				<div class="wFormInput">
					<input class="wInput" type="text" name="order-user-name" id="order-user-name" required data-rule-word="true" data-rule-minlength="2"  placeholder="Ваше имя">
				</div>
			</div>
			<div class="wFormRow">
				<small>Ваше номер<span class="_color-danger">*</span></small>
				<div class="wFormInput">
					<input class="wInput js-inputmask" type="text" name="order-user-phone" id="order-user-phone" required data-rule-pattern="\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}" data-msg-pattern="Укажите корректный номер" placeholder="+7 (___) ___-__-__">
				</div>
			</div>
		</div>
		<div class="wFormRow" style="max-width: 310px;">
			<button class="button button--primary button--noround button--expand wSubmit"><span>Заказать оригинальную шаль</span></button>
		</div>
	</div>
</div>
