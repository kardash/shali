<?php
    namespace Core;

    use Modules\Catalog\Models\Groups;
    use Modules\Catalog\Models\Items;
    use Modules\News\Models\News;
    use Core\QB\DB;
    use Modules\Cart\Models\Cart;
    use Modules\Catalog\Models\Filter;

    /**
     *  Class that helps with widgets on the site
     */
    class Widgets {

        static $_instance; // Constant that consists self class

        public $_data = array(); // Array of called widgets
        public $_tree = array(); // Only for catalog menus on footer and header. Minus one query

        // Instance method
        static function factory() {
            if(self::$_instance == NULL) { self::$_instance = new self(); }
            return self::$_instance;
        }

        /**
         *  Get widget
         *  @param  string $name  [Name of template file]
         *  @param  array  $array [Array with data -> go to template]
         *  @return string        [Widget HTML]
         */
        public static function get( $name, $array = array(), $save = true, $cache = false ) {
            $arr = explode('_', $name);
            $viewpath = implode('/', $arr);

            if( APPLICATION == 'backend' && !Config::get('error') ) {
                $w = WidgetsBackend::factory();
            } else {
                $w = Widgets::factory();
            }

            $_cache = Cache::instance();
            if($cache) {
                if (!$_cache->get($name)) {
                    $data = NULL;
                    if ($save && isset($w->_data[$name])) {
                        $data = $w->_data[$name];
                    } else {
                        if( $save && isset( $w->_data[ $name ] ) ) {
                            $data = $w->_data[ $name ];
                        } else if( method_exists( $w, $name ) ) {
                            $result = $w->$name($array);
                            if( $result !== NULL && $result !== FALSE ) {
                                $array = array_merge($array, $result);
                                $data = View::widget( $array, $viewpath);
                            } else {
                                $data = NULL;
                            }
                        } else {
                            $data = $w->common( $viewpath, $array );
                        }
                    }
                    $_cache->set($name, HTML::compress($data, true));
                    return $w->_data[$name] = $data;
                } else {
                    return $_cache->get($name);
                }
            }
            if($_cache->get($name)) {
                $_cache->delete($name);
            }
            if( $save && isset( $w->_data[ $name ] ) ) {
                return $w->_data[ $name ];
            }
            if( method_exists( $w, $name ) ) {
                $result = $w->$name($array);
                if( $result !== NULL && $result !== FALSE ) {
                    if(is_array($result)) {
                        $array = array_merge($array, $result);
                    }
                    return $w->_data[$name] = View::widget( $array, $viewpath);
                } else {
                    return $w->_data[$name] = NULL;
                }
            }
            return $w->_data[$name] = $w->common( $viewpath, $array );
        }

        /**
         *  Common widget method. Uses when we have no widgets called $name
         *  @param  string $viewpath  [Name of template file]
         *  @param  array  $array     [Array with data -> go to template]
         *  @return string            [Widget HTML or NULL if template doesn't exist]
         */
        public function common( $viewpath, $array ) {
            if( file_exists(HOST.'/Views/Widgets/'.$viewpath.'.php') ) {
                return View::widget($array, $viewpath);
            }
            return NULL;
        }


        public function HiddenData() {
            $cart = Cart::factory()->get_list_for_basket();
            return array( 'cart' => $cart );
        }

        public function Footer() {
            $contentMenu = Common::factory('sitemenu')->getRows(1, 'sort');
            $array['contentMenu'] = $contentMenu;
            return $array;
        }


        public function Header() {
			$items = Common::factory('catalog')->getRows(1, 'sort', 'ASC', 8);
			$item = Common::getRow(Route::param('id'), 'id', 1);
            $array['items'] = $items;
			$array['obj'] = $item;
            return $array;
        }


        public function Head() {
            $styles = array(
                HTML::media('css/fonts.css'),
                HTML::media('css/libs.css'),
                HTML::media('css/style.css'),
                HTML::media('css/programmer/my.css'),
                HTML::media('css/programmer/fpopup.css'),
            );
            $scripts = array(
                HTML::media('js/modernizr.js')
            );
            $scripts_no_minify = array(
                HTML::media('js/programmer/ulogin.js'),
            );
            return array('scripts' => $scripts, 'styles' => $styles);
        }
		
		 public function Index_Slider() {
            $result = Common::factory('slider')->getRows(1, 'sort', 'ASC');
            if( !sizeof( $result ) ) {
                return FALSE;
            }
            return array( 'result' => $result );
        }
		
		 public function Index_Assortment() {
            $result = DB::select('catalog.*')
                ->from('catalog')
                ->where('catalog.status', '=', 1)
                ->order_by('catalog.sort','ASC')
				->limit(Config::get('basic.limit'))
                ->find_all();
            if( !sizeof($result) ) {
                return FALSE;
            }
            return array( 'result' => $result );
        }
		
		public function Index_Reviews() {
            $result = Common::factory('reviews')->getRows(1, 'id', 'DESC');
            if( !sizeof($result) ) {
                return FALSE;
            }
            return array( 'result' => $result );
        }
		
		public function Index_Unpacking() {
            $result = DB::select('gallery_images.image')
                ->from('gallery')
				->join('gallery_images', 'LEFT')->on('gallery_images.gallery_id','=','gallery.id')
                ->where('gallery.status', '=', 1)
                ->where('gallery.id', '=', 5)
                ->order_by('gallery_images.sort','ASC')
                ->find_all();
            if( !sizeof($result) ) {
                return FALSE;
            }
            return array( 'result' => $result );
        }
		
		public function Index_OrderShawl() {
            $result = Common::factory('address')->getRows(1, 'sort', 'ASC');
            if( !sizeof($result) ) {
                return FALSE;
            }
            return array( 'result' => $result );
        }
		
		public function Navigation() {
			$result = Common::factory('catalog')->getRows(1, 'sort', 'ASC', 8);
			$item = Common::getRow(Route::param('id'), 'id', 1);
            $array['obj'] = $item;
            $array['result'] = $result;
            return $array;
        }
		
		 public function Item_ItemsSame() {
            $result = DB::select('catalog.*')
                        ->from('catalog')
                        ->where('catalog.status', '=', 1)
                        ->where('catalog.id', '!=', Route::param('id'))
                        ->order_by(DB::expr('rand()'))
                        ->limit(Config::get('basic.limit_same'))
                        ->find_all();
            if( !sizeof($result) ) {
                return FALSE;
            }
            return array( 'result' => $result);
        }
		


    }