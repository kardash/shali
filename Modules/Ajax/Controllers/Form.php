<?php
    namespace Modules\Ajax\Controllers;

    use Core\GeoIP;
    use Core\QB\DB;
    use Core\Arr;
    use Core\User;
    use Core\Config AS conf;
    use Core\View;
    use Core\System;
    use Modules\Cart\Models\Cart;
    use Core\Log;
    use Core\Email;
    use Core\Message;
    use Core\Common;
	 use Modules\Catalog\Models\Items;

    class Form extends \Modules\Ajax {

        protected $post;
        protected $files;

        function before() {
            parent::before();
            // Check for bans in blacklist
            $ip = GeoIP::ip();
            $ips = array();
            $ips[] = $ip;
            $ips[] = $this->ip($ip, array(0));
            $ips[] = $this->ip($ip, array(1));
            $ips[] = $this->ip($ip, array(1,0));
            $ips[] = $this->ip($ip, array(2));
            $ips[] = $this->ip($ip, array(2,1));
            $ips[] = $this->ip($ip, array(2,1,0));
            $ips[] = $this->ip($ip, array(3));
            $ips[] = $this->ip($ip, array(3,2));
            $ips[] = $this->ip($ip, array(3,2,1));
            $ips[] = $this->ip($ip, array(3,2,1,0));
            if( count($ips) ) {
                $bans = DB::select('date')
                    ->from('blacklist')
                    ->where('status', '=', 1)
                    ->where('ip', 'IN', $ips)
                    ->and_where_open()
                        ->or_where('date', '>', time())
                        ->or_where('date', '=', NULL)
                    ->and_where_close()
                    ->find_all();
                if(sizeof($bans)) {
                    $this->error('К сожалению это действие недоступно, т.к. администратор ограничил доступ к сайту с Вашего IP адреса!');
                }
            }
        }
        private function ip($ip, $arr) {
            $_ip = explode('.', $ip);
            foreach($arr AS $pos) {
                $_ip[$pos] = 'x';
            }
            $ip = implode('.', $_ip);
            return $ip;
        }
		
		        // Send callback
        public function callbackAction() {
            // Check incoming data
            $phone = trim(Arr::get($this->post, 'phone'));
            $time = strtotime(Arr::get($this->post, 'time'));
          
            // Check for bot
            $ip = System::getRealIP();
            $check = DB::select(array(DB::expr('COUNT(callback.id)'), 'count'))
                ->from('callback')
                ->where('ip', '=', $ip)
                ->where('created_at', '>', time() - 60)
                ->as_object()->execute()->current();
            if( is_object($check) AND $check->count ) {
                $this->error('Нельзя так часто просить перезвонить! Пожалуйста, повторите попытку через минуту');
            }

            // Save callback
            $lastID = DB::insert('callback', array('phone', 'ip', 'status', 'created_at', 'time'))->values(array($phone, $ip, 0, time(), $time))->execute();
            $lastID = Arr::get($lastID, 0);

            // Save log
            $qName = 'Заказ звонка';
            $url = '/wezom/callback/edit/' . $lastID;
            Log::add( $qName, $url, 3 );

            // Send E-Mail to admin
            $mail = DB::select()->from('mail_templates')->where('id', '=', 3)->where('status', '=', 1)->as_object()->execute()->current();
            if( $mail ) {
                $from = array( '{{site}}', '{{ip}}', '{{date}}', '{{time}}', '{{phone}}' );
                $to = array(
                    Arr::get( $_SERVER, 'HTTP_HOST' ), $ip, date('d.m.Y H:i'),
                    date('H:i',$time), $phone
                );
                $subject = str_replace($from, $to, $mail->subject);
                $text = str_replace($from, $to, $mail->text);
                Email::send( $subject, $text );
            }

            $this->success( 'Администрация сайта скоро Вам перезвонит!' );
        }
		
		
        public function orderShawlAction(){
            // Check incomming data
			$phone = trim(Arr::get($this->post, 'phone'));
            $name = trim(Arr::get($this->post, 'name'));
			$catalog_id = trim(Arr::get($this->post, 'catalog_id'));
			$color = trim(Arr::get($this->post, 'color'));
			
			if ($catalog_id) {
				$item = Items::getRow($catalog_id, 'id', 1);
			}


            // Check for bot
            $ip = System::getRealIP();
            $check = DB::select(array(DB::expr('orders_simple.id'), 'count'))
                ->from('orders_simple')
                ->where('ip', '=', $ip);
			if ($catalog_id) {
				$check = $check->where('catalog_id', '=', $id);
			}
            $check = $check->where('created_at', '>', time() - 60)
                ->as_object()->execute()->current();
            if( is_object($check) AND $check->count ) {
                $this->error('Вы только что оформили заказа! Пожалуйста, повторите попытку через минуту');
            }

            // All ok. Save data
            $keys = array('ip', 'phone', 'name','catalog_id', 'color','created_at');
            $values = array($ip, $phone, $name, $item->id, $color, time());
            $lastID = DB::insert('orders_simple', $keys)->values($values)->execute();
            $lastID = Arr::get($lastID, 0);

            // Create links
            $link = 'http://' . Arr::get( $_SERVER, 'HTTP_HOST' ) . '/'.$item->alias.'/p'.$item->id;
            $link_admin = 'http://' . Arr::get( $_SERVER, 'HTTP_HOST' ) . '/wezom/simple/edit/' . $lastID;;

            // Save log
            $qName = 'Заказ шали';
            $url = '/wezom/simple/edit/' . $lastID;
            Log::add( $qName, $url, 7 );

            // Send message to admin if need
            $mail = DB::select()->from('mail_templates')->where('id', '=', 8)->where('status', '=', 1)->as_object()->execute()->current();
            if( $mail ) {
                $from = array( '{{site}}', '{{ip}}', '{{date}}', '{{phone}}', '{{name}}', '{{color}}','{{link}}', '{{link_admin}}', '{{item_name}}' );
                $to = array(
                    Arr::get( $_SERVER, 'HTTP_HOST' ), $ip, date('d.m.Y H:i'),
                    $phone, $name, $color ? $color: '---', $item->name ? $link : '#' , $link_admin, $item->name ? $item->name : '---',
                );
                $subject = str_replace($from, $to, $mail->subject);
                $text = str_replace($from, $to, $mail->text);
                Email::send( $subject, $text );
            }
            $this->success('Вы успешно оформили заказ! Администрация сайта свяжется с Вами в ближайшее время');
        }

}