<?php
    namespace Modules\Ajax\Controllers;

    use Core\Arr;
    use Core\QB\DB;
    use Core\User;
    use Core\Widgets;
    use Core\CommonI18n;
    use Core\Config;
    use Modules\Catalog\Models\Cert;
    use Modules\Catalog\Models\Items;
    use Wezom\Modules\Dict\Models\Colors;
    use Wezom\Modules\Dict\Models\Sizes;
	use Modules\Branches\Models\Branches;
	use Modules\Catalog\Models\Groups;
	

    class Popup extends \Modules\Ajax {

       
		
		public function callbackAction() {
			echo Widgets::get('Popup/Callback');
        }
		
		
		public function orderItemAction() {
			$catalog_id = (int) Arr::get($_POST, 'id');
			$image_id = trim(Arr::get($_POST, 'image_id'));
			if ($catalog_id) {
				$item = Items::getRowForOrder($catalog_id, $image_id);
				$cost = ceil($item->cost - $item->cost*$item->discount/100);
			}			
            echo Widgets::get('Popup/OrderItem', array('item' => $item,'cost' => $cost));
        }

        public function after() {
            die;
        }

    }