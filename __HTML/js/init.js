'use-strict';

var _transitFlag = Modernizr.cssanimations;

/**
 * @namespace whtml
 * @see {@link wHTML}
 */

/**
 * @alias wHTML
 * @see {@link whtml}
 */
window.wHTML = (function ($) {

    // ПРИВАТНЫЕ МЕТОДЫ И ПЕРЕМЕННЫЕ
    // -----------------------------

    /**
     * Проверка данных на тип `object`
     * @type {Null}
     * @memberof whtml
     * @sourcecode
     * @inner
     * @param {*} data - образец для проверки
     * @return {Boolean} data - образец для проверки
     */
    function _isObject(data) {
        return (typeof data == 'object') && (data !== null);
    }

    // ПУБЛИЧНЫЕ МЕТОДЫ
    // ----------------

    /**
     * @memberof whtml
     * @constructor
     */
    function _mtds() {
    };

    /**
     * @memberof whtml._mtds
     * @param {String=} selector The selector
     * @sourcecode
     * @example
     * // magnificPopup inline
     *    wHTML.mfi();
     */
    _mtds.prototype.inputMask = function ($where) {
        var $masks = $where.find('.js-inputmask');
        if ($masks.length) {
            $masks.each(function (index, el) {
                var $el = $(el);
                $el.inputmask({
                    mask: [{
                        "mask": $el.data('mask') || "+7 (###) ###-##-##"
                    }],
                    definitions: {
                        '#': {
                            validator: "[0-9]",
                            cardinality: 1
                        }
                    }
                });
            });
        }
    };

    /**
     * @memberof whtml._mtds
     * @param {String=} selector The selector
     * @sourcecode
     * @example
     * // magnificPopup iframe
     *    wHTML.mfiFrame();
     */

    _mtds.prototype.mfiFrame = function (selector) {
        selector = selector || '.mfiF';
        if ($(selector).length) {
            $(selector).magnificPopup({
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 300,
				preloader: false,
				fixedContentPos: false
            });
        }
    };

	/**
	 * @memberof whtml._mtds
	 * @param {String=} selector The selector
	 * @sourcecode
	 * @example
	 * // magnificPopup inline
	 *    wHTML.mfi();
	 */
	_mtds.prototype.mfi = function (selector) {
		selector = selector || '.mfi';
		if ($(selector).length) {
			$(selector).magnificPopup({
				type: 'inline',
				closeBtnInside: true,
				removalDelay: 300,
				mainClass: 'zoom-in'
			});
		}
	};

    /**
     * @memberof whtml._mtds
     * @param {String=} selector The selector
     * @sourcecode
     * @example
     * // magnificPopup ajax
     *    wHTML.mfiAjax();
     */
    _mtds.prototype.mfiAjax = function (selector) {
        selector = selector || '.mfiA';
        $('body').magnificPopup({
            delegate: selector,
            callbacks: {
                elementParse: function (item) {
                    this.st.ajax.settings = {
                        url: item.el.data('url'),
                        type: 'POST',
                        data: (typeof item.el.data('param') !== 'undefined') ? item.el.data('param') : ''
                    };
                },
                ajaxContentAdded: function (el) {
                    wHTML.inputMask(this.container.eq(0));
                    wHTML.validation();
                }
            },
            type: 'ajax',
            removalDelay: 300,
            mainClass: 'zoom-in'
        });
    };

    /**
     * @memberof whtml._mtds
     * @sourcecode
     * @example
     * // фиксим iframe'ы в текстах
     *    wHTML.respIframes();
     */
    _mtds.prototype.respIframes = function () {
        var list = $('.page-text').find('iframe').add($('.page-text').find('video'));
        if (list.length) {
            for (var i = 0; i < list.length; i++) {
                var element = list[i];
                var jqElement = $(element);
                if (jqElement.hasClass('ignoreHolder')) continue;
                if (typeof jqElement.data('wraped') === 'undefined') {
                    jqElement.data('wraped', true).wrap('<div class="iframeHolder ratio_16x9" style="padding-top:56.25%;""></div>');
                }
            }
        }
    };

    /**
     * @memberof whtml._mtds
     * @param {String=} - контекст инициализации
     * @sourcecode
     * @example
     * // инициализируем выпадающий список
     *    wHTML.dropdownInit();
     */
    _mtds.prototype.dropdownInit = function (context) {
        context = context || 'body';
        $('[data-dropdown]', context).wzmClassToggle({
            toggleSelector: '[data-dropdown-toggle]',
            toggleOnBlur: true
        });
    };

    /**
     * @memberof whtml._mtds
     * @param {String=} - контекст инициализации
     * @sourcecode
     * @example
     * // инициализируем переключения класса у body
     *    wHTML.bodyToggleInit();
     */

    _mtds.prototype.bodyToggleInit = function (context) {
        context = context || 'body';
        $('[data-body-toggle-class]', context).wzmBodyClassToggle();
    };

    /**
     * @memberof whtml._mtds
     * @param {String=} - контекст инициализации
     * @sourcecode
     * @example
     * // инициализируем табы
     *    wHTML.tabsInit();
     */

    _mtds.prototype.tabsInit = function (context) {
        context = context || 'body';
        if ($('.js-tab-toggle', context).length) {
            $('.js-tab-toggle', context).on('click', function () {
                var $this = $(this);

                var
                    tabset = $this.data('tabset'),
                    tabname = $this.data('tabname');

                $('.js-tab-content[data-tabset="' + tabset + '"]').each(function (i, el) {
                    var $el = $(el);
                    if ($el.data('tabname') == tabname) {
                        $el.addClass('is-active');
                    } else {
                        $el.removeClass('is-active');
                    }
                    if ($el.hasClass('is-active')) {
                        $('.js-tab-toggle[data-tabset="' + tabset + '"]').removeClass('is-active');
                        $this.addClass('is-active');
                    }
                });
            });
        }
    };

    _mtds.prototype.carouselInit = function () {
        $('.js-carousel').each(function () {
            var
                $ths = $(this),
                presets = {
                    main: {
                        speed: 1600,
                        autoplay: true,
                        autoplaySpeed: 3000,
                        pauseOnHover: false,
                        responsive: [
                            {
                                breakpoint: 1280,
                                settings: {
                                    speed: 800
                                }
                            }
                        ]
                    },
                    round_main: {
                        arrows: false,
                        fade: true,
                        asNavFor: '.js-carousel[data-preset="round_navigation"]'
                    },
                    round_navigation: {
                        slidesToShow: 4,
                        asNavFor: '.js-carousel[data-preset="round_main"]',
                        focusOnSelect: true,
                        responsive: [
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3
                                }
                            },
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 2
                                }
                            }
                        ]
                    }
                },
                preset = presets[$ths.data('preset')];

            $ths.slick($.extend(
                {
                    adaptiveHeight: true,
                    swipeToSlide: true
                },
                preset));
        });
    };

    _mtds.prototype.gotoAnchor = function (target, offset, duration, cb) {
        offset = offset ? offset : 0;
		duration = duration || 1000;
        if (location.pathname.replace(/^\//, '') == target.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
            target = $(target.hash);
            target = target.length ? target : $('[name=' + target.hash.slice(1) + ']');
            $('html,body').stop().animate({
                scrollTop: target.offset().top + offset
            }, {
                duration: duration,
                complete: function () {
                    if (cb && typeof cb === "function") {
                        cb();
                    }
                }
            });
            return false;
        } else {
            e.preventDefault();
            return false;
        }
    };

    _mtds.prototype.screenShift = function (anchorsContainer) {
        function _screenWatch() {
            var
                windowPosition = $(window).scrollTop(),
                windowHeight = $(window).height(),
                docHeight = $(document).height();

            for (var i = 0; i < anchorsMap.length; i++) {
                var
                    screenId = anchorsMap[i],
                    screenPosition = $(screenId).offset().top - $('.pageHeader').outerHeight() - 50,
                    screenHeight = $(screenId).outerHeight(true);

                if (windowPosition > screenPosition && windowPosition < (screenPosition + screenHeight)) {
                    $anchors.filter('[href="' + screenId + '"]').addClass("is-active");
                } else {
                    $anchors.filter('[href="' + screenId + '"]').removeClass("is-active");
                }
            }

            if (windowPosition + windowHeight == docHeight) {
                var $lastAnchor = $anchorsContainer.find('a[href="' + anchorsMap[anchorsMap.length - 1] + '"]');
                if (!$lastAnchor.hasClass("is-active")) {
                    $anchors.filter('.is-active[href]').removeClass("is-active");
                    $lastAnchor.addClass("is-active");
                }
            }
        }

        if ($(anchorsContainer).length) {
            var
                $anchorsContainer = $(anchorsContainer),
            	$anchors = $anchorsContainer.find('a');

            var anchorsMap = [];

            $anchors.each(function () {
				var href = $(this).attr('href');
				if (anchorsMap.indexOf(href) === -1) {
				 	anchorsMap.push(href);
				}
            });

            _screenWatch();

            $anchorsContainer.on('click', 'a', function (e) {
                e.preventDefault();

                var
					that = this,
					$parent = $(e.delegateTarget);

                function scrollEnd() {
                    $parent.removeClass('is-scrolling');
					wHTML.gotoAnchor(that,$('.pageHeader').outerHeight()*-1,400);
                }

                $parent.addClass('is-scrolling');

                wHTML.gotoAnchor(that,$('.pageHeader').outerHeight()*-1,false,scrollEnd);
            });

            $(window).scroll(function () {
                if ($anchorsContainer.is(':visible')) {
                    _screenWatch();
                }
            });
        }
    };

    _mtds.prototype.pageHeaderStates = function () {
        var
            $pageHeader = $('.pageHeader'),
            headerHeight = $pageHeader.outerHeight();

        $pageHeader.addClass('is-startPosition');

		function setState() {
            if ($(window).scrollTop() >= headerHeight + 100) {
                $pageHeader.removeClass('is-startPosition');
                $('body').addClass('has-fixedHeader');
                $('.pageWrapper').css({
                    'padding-top': headerHeight
                });
            } else {
                $('body').removeClass('has-fixedHeader');
                $('.pageWrapper').css({
                    'padding-top': 0
                });
            }
		}
		setState();

        $(window).resize(function () {
            headerHeight = $('.pageHeader').outerHeight();
        });

        $(window).scroll(function () {
			setState();
        });
    };

    return new _mtds();

})(jQuery);

jQuery(document).ready(function ($) {
    // onREADY
    // ------
    initializeTabbedMaps();
    initializeSourceChanger();

    wHTML.inputMask($(document));

    // очитска localStorage
    localStorage.clear();

    // magnificPopup inline
    wHTML.mfi();

    // magnificPopup ajax
    wHTML.mfiAjax();

	// magnificPopup iframe
	wHTML.mfiFrame();

    // dropdownInit
    wHTML.dropdownInit();

    // bodyToggleInit
    wHTML.bodyToggleInit();

    // tabsInit
    wHTML.tabsInit();

    // carouselInit
    wHTML.carouselInit();

    // onLOAD
    // ------
    $(window).load(function () {

		// перелистывание экранов
		wHTML.screenShift('.js-screenshift');

        // фиксим iframe'ы в текстах
        wHTML.respIframes();

		// Изменения состояния хедера
		wHTML.pageHeaderStates();

        // инитим валидацию
        wHTML.validation();

    });

});

function initializeSourceChanger() {
    $('[data-init="dataChanger"]').each(function (i, container) {
        var
            $container = $(container),
            sPluginID = 'data-changer',
            oNodes = collectPluginNodes($container,sPluginID);
        // i==0?console.log(oNodes):false;
        $container.on('click',"[data-"+sPluginID+"='sourceImage']",function(e){
            var
                $this = $(this),
                oInfo = $this.data('info');
            oNodes.$sourceImage.removeClass('is-active');
            $this.addClass('is-active');
            oNodes.$destinationImage.attr('src',oInfo.src);
            oNodes.$oldPrice.html(formatDigits(oInfo.oldPrice,'’'));
            oNodes.$price.html(formatDigits(oInfo.price,'’'));
            oNodes.$color.html(oInfo.color);
        });
    });
}
/**
 * Разделение числа по разрядам
 * @param number
 * @param {string} [delimiter=&nbsp;] - Разделитель
 * @returns {string}
 */
function formatDigits(number, delimiter){
    delimiter = delimiter || '&nbsp;';
    return (number+'').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1'+delimiter);
}
/**
 * Возвращает объект с jQuery объектами, являющиеся частями плагина.
 * @param $container - Контейнер, по которому проинициализировался плагин
 * @param pluginID - Индентификатор плагина
 * @returns {{}}
 */
function collectPluginNodes($container,pluginID) {
    var oNodes ={};
    $container.find("[data-"+ pluginID +"]").each(function () {
        var $node = $(this),
            sNodeSelector = '$' + $node.data(pluginID);
        if (oNodes[sNodeSelector]){
            oNodes[sNodeSelector].push($node[0]);
        } else {
            oNodes[sNodeSelector] = $node;
        }

    });
    return oNodes;
}
/**
 * Инициализирует карту гугла с кнопками - переключения координат
 */
function initializeTabbedMaps() {
    $('[data-init="tabbedMap"]').each(function (i, container) {
        var
            $container = $(container),
            oNodes = collectPluginNodes($container,'tabbedmap');

        var oMap = initMap(oNodes.$map, getCoords(oNodes.$initial));
        oNodes.$controls.children().each(function (i, button) {
            var
                $button = $(button),
                sInfoWindowID = $button.data('infowindow');
            var oInfoWindow = new google.maps.InfoWindow({
                content: $('#' + sInfoWindowID)[0].outerHTML
            });
            var oMarker = setMarker(oMap, getCoords($(this)));
            $button.data('oInfoWindow', oInfoWindow);
            $button.data('oMarker', oMarker);
        });
        oNodes.$controls.on("click", "button", function () {
            changePosition($(this), oMap);
        });
        changePosition(oNodes.$initial, oMap);

    });
}
/**
 * Возвращает новый экземпляр объекта карты. и записывает его в $map.data
 * @param $map - Контейнер, в котором будет находиться карта.
 * @param [crdsCenter] - Координаты центра карты.
 * @returns {google.maps.Map}
 */
function initMap($map, crdsCenter) {
    // Create a map object and specify the DOM element for display.
    var centerCoordindates = crdsCenter ?{center:{lat: crdsCenter.lat, lng: crdsCenter.lng}}:{};
    var oMap = new google.maps.Map($map[0], $.extend({

        scrollwheel: false,
        zoom: 14
    },centerCoordindates));
    $map.data('oMap', oMap);
    return oMap;

}
/**
 * Возвращает координаты из дата-атрибутов элемента.
 * Считывает data-lng и data-lat.
 * @param $element - Элемент для считывания коодинат
 * @returns {{lat: *, lng: *}}
 */
function getCoords($element) {
    return {
        lat: $element.data('lat'),
        lng: $element.data('lng')
    }
}
/**
 * Устанавливает маркер и возвращает его объект.
 * @param {{}} oMap - Карта
 * @param {{}} oCoords - Координаты маркера.
 * @returns {google.maps.Marker}
 */
function setMarker(oMap, oCoords) {
    var oMarker = new google.maps.Marker({
        position: oCoords,
        map: oMap
    });
    return oMarker;
}
/**
 * Перемещения карты к координатам прописаным в кнопке.
 * @param {$} $button - Кнопка - содержащая коодинаты.
 * @param {{}} oMap - Карта.
 */
function changePosition($button, oMap) {
    var
        oMarker = $button.data('oMarker'),
        oInfoWindow = $button.data('oInfoWindow');

    oMap.panTo(getCoords($button));
    oMap.panBy(220, -110);
    setTimeout(function () {
        oInfoWindow.open(oMap, oMarker)
    }, 800)
}