<div class="mfiModal mfiModal--form">
	<button title="Закрыть (ESC)" type="button" class="mfp-close">&times;</button>
	<div class="pageTitle">Заказать<br>обратный звонок</div>
	<div class="wTxt">
		<p>Наш менеджер свяжется с вами в течении 5 минут</p>
	</div>
	<div class="wForm wFormDef" data-form="true">
		<div class="wFormRowParter _clear-after">
			<div class="wFormRow">
				<small>Ваше номер<span class="_color-danger">*</span></small>
				<div class="wFormInput _with-arrow">
					<input class="wInput js-inputmask" type="text" name="callback-user-phone" id="callback-user-phone" required data-rule-pattern="\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}" data-msg-pattern="Укажите корректный номер" placeholder="+7 (___) ___-__-__">
				</div>
			</div>
			<div class="wFormRow">
				<small>Время звонка<span class="_color-danger">*</span></small>
				<div class="wFormInput">
					<select class="wInput" name="callback-call-time" id="callback-call-time">
						<option value="8">8:00</option>
						<option value="9">9:00</option>
						<option value="10" selected>10:00</option>
						<option value="11">11:00</option>
						<option value="12">12:00</option>
						<option value="13">13:00</option>
						<option value="14">14:00</option>
						<option value="15">15:00</option>
						<option value="16">16:00</option>
						<option value="17">17:00</option>
						<option value="18">18:00</option>
						<option value="19">19:00</option>
						<option value="20">20:00</option>
						<option value="21">21:00</option>
						<option value="22">22:00</option>
					</select>
				</div>
			</div>
		</div>
		<div class="wFormRow" style="max-width: 310px;">
			<button class="button button--primary button--noround button--expand wInputwSubmit wSubmit"><span>Заказать звонок</span></button>
		</div>
	</div>
</div>
