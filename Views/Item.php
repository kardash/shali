<!DOCTYPE html>
<html lang="ru-ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Core\Widgets::get('Head', $_seo); ?>
    <?php foreach ( $_seo['scripts']['head'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body class="item" >

	<div class="pageWrapper">
		<?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>

		<div class="pageMain ">
			<div class="pageSize">
				<?php echo $_content;?>
			</div>
		</div>
		<?php echo Core\Widgets::get('Footer', array('counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config)); ?>
	</div>
	<!-- /pageWrapper -->
	<?php echo Core\Widgets::get('Navigation'); ?> 
</body>

</html>