<!DOCTYPE html>
<html lang="ru-ru" dir="ltr">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Core\Widgets::get('Head', $_seo); ?>
    <?php foreach ( $_seo['scripts']['head'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body class="index" >

	<div class="pageWrapper">
		
		<section class="pageSection pageSection--background-gray pageSection--seo">
			<div class="pageSection__inner">
				<div class="wTxt seoBlock__text">
					<?php echo $_content;?>
				</div>
			</div>
		</section>
		
		<?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>
			
		<div class="pageMain ">
			<div class="pageSize">
				<?php echo Core\Widgets::get('Index_Slider'); ?>
				<?php echo Core\Widgets::get('Index_Gallery'); ?>				
				<?php echo Core\Widgets::get('Index_Original'); ?>
				<?php echo Core\Widgets::get('Index_Unpacking'); ?>
				<?php echo Core\Widgets::get('Index_Assortment'); ?>
				<?php echo Core\Widgets::get('Index_WhyWe'); ?>
				<?php echo Core\Widgets::get('Index_Work'); ?>			
				<?php echo Core\Widgets::get('Index_Reviews'); ?>	
				<?php echo Core\Widgets::get('Index_OrderShawl'); ?>	
			</div>
		</div>
		<?php echo Core\Widgets::get('Footer', array('counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config)); ?>
		<?php echo Core\Widgets::get('HiddenData'); ?>
	</div>
	<!-- /pageWrapper -->
	<?php echo Core\Widgets::get('Navigation'); ?>

</body>

</html>