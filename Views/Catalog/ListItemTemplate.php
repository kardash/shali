<?php $cost = ceil($obj->cost - $obj->cost*$obj->discount/100);?>
<?php $images = \Modules\Catalog\Models\Items::getItemImages($obj->id);?>
<div class="productCard__gallery">
	<?php if (count($images)):?>
		<?php foreach ($images as $im):?>
			<?php if ($im->main == 1 ):?>
				<?php $color = $im->color;?>
				<a href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" class="productCard__imageContainer" data-pseudo-button="Подробнее" title="<?php echo $obj->name;?>">
					<?php if( is_file(HOST.Core\HTML::media('images/catalog/medium/'.$im->image)) ): ?>
						<img src="<?php echo \Core\HTML::media('images/catalog/medium/'.$im->image);?>" data-data-changer="destinationImage">
					<?php else:?>
						<img src="<?php echo Core\HTML::media('pic/no-image.png'); ?>" data-data-changer="destinationImage">
					<?php endif;?>
				</a>
			<?php endif;?>
		<?php endforeach;?>
	<?php endif;?>
	
	
	<div class="productCard__thumbnailContainer">
		<?php if (count($images)):?>
			<?php foreach ($images as $im): ?>
				<?php if( is_file(HOST.Core\HTML::media('images/catalog/medium/'.$im->image)) ): ?>
				<div data-data-changer="sourceImage"
					 data-info='{
					 "src":"<?php echo \Core\HTML::media('images/catalog/medium/'.$im->image);?>",
					 "price":<?php echo $cost;?>,
					 "oldPrice":<?php echo $obj->cost;?>,
					 "color":"<?php echo $im->color;?>"}'
					 class="productCard__thumbnail <?php if ($im->main==1) echo 'is-active';?>">
					<img src="<?php echo \Core\HTML::media('images/catalog/medium/'.$im->image);?>" >
				</div>
				<?php endif;?>
			<?php endforeach;?>
		<?php endif;?>
	</div>
</div>
<div class="productCard__info">
	<div class="productCard__infoUp">
		<a href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" class="productCard__title" title="<?php echo $obj->name;?>"><?php echo $obj->name;?></a>
		<div class="productCard__subtitle" data-data-changer="color"><?php echo $color;?></div>
	</div>
	<div class="productCard__infoDown">
		<div class="productCard__featureText">Цена в фирменных магазинах</div>
		<div class="productCard__price productCard__price--old"><span data-data-changer="oldPrice"><?php echo number_format($obj->cost, 0, "", "’");?></span> ₽</div>
		<div class="productCard__featureText _font-bold">Ваша цена</div>
		<div class="productCard__price"><span data-data-changer="price"><?php echo number_format($cost, 0, "", "’");?></span> ₽</div>
		<button class="button button--primary button--noround button--expand" title="Купить"><span>Купить</span></button>
	</div>
</div>