<?php /*<div class="fll">
    <div class="prevue_block clearFix">
        <div class="fll">
            <div class="prev3"><span></span></div>
            <div class="next3"><span></span></div>
            <ul>
                <?php foreach( $images as $im ): ?>
                    <?php if( is_file(HOST.Core\HTML::media('images/catalog/small/'.$im->image)) ): ?>
                        <li>
                            <div class="img_prevue" data-img-src="<?php echo Core\HTML::media('images/catalog/big/'.$im->image); ?>" data-img-src-original="<?php echo Core\HTML::media('images/catalog/original/'.$im->image); ?>">
                                <img src="<?php echo Core\HTML::media('images/catalog/small/'.$im->image); ?>" />
                            </div>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="prevue_block_in">
            <div class="big_prevue fresco" data-fresco-group="gr1" href="<?php echo Core\HTML::media('images/catalog/original/'.$images[0]->image) ?>">
                <?php if( is_file(HOST.Core\HTML::media('images/catalog/big/'.$images[0]->image)) ): ?>
                    <img src="<?php echo Core\HTML::media('images/catalog/big/'.$images[0]->image); ?>" alt="" />
                <?php endif; ?>
                <?php echo Core\Support::addItemTag($obj); ?>
            </div>
        </div>
    </div>
    <?php echo Core\Widgets::get('Socials'); ?>
    <?php echo Core\Widgets::get('Item_Comments'); ?>
</div>
<div class="flr">
    <div class="tovar_info">
        <div class="tovar_name"><h1><?php echo Core\Config::get('h1'); ?></h1></div>
        <?php if ($obj->sale): ?>
            <div class="old_price2">
                <span><?php echo $obj->cost_old; ?></span> грн
            </div>
        <?php endif ?>
        <div class="tovar_price">
            <?php echo $obj->cost; ?> <span>грн</span>
        </div>
        <?php if( $obj->available == 0 ): ?>
            <div class="tovar_vNall">
                <p>товара нет в наличии</p>
            </div>
        <?php endif; ?>
        <?php if( $obj->available == 1 ): ?>
            <div class="tovar_vNall">
                <div class="vNall_img"></div>
                <p>товар в наличии</p>
                <p><?php echo Core\Config::get('static.dostavka_est'); ?></p>
            </div>
        <?php endif; ?>
        <?php if( $obj->available == 2 ): ?>
            <div class="tovar_vNall noNall">
                <div class="vNall_img"></div>
                <p>товар под заказ</p>
                <?php echo Core\Config::get('static.dostavka_bron'); ?>
            </div>
        <?php endif; ?>
        <div class="sel_but clearFix">
            <div class="fll"></div>
            <div class="flr">
                <a href="#" class="buy_but addToCart" data-id="<?php echo $obj->id; ?>"><span>В КОРЗИНУ</span></a>
                <a href="#enterReg5" class="popup_but enterReg5" data-id="<?php echo $obj->id; ?>"><span>КУПИТЬ В ОДИН КЛИК</span></a>
            </div>
        </div>
    </div>
    <div class="tovar_info2">
        <div class="middle_title">характеристики товара</div>
        <div class="har_table">
            <table>
                <?php if( $obj->artikul ): ?>
                    <tr>
                        <td>артикул</td>
                        <td><?php echo $obj->artikul; ?></td>
                    </tr>
                <?php endif; ?>
                <?php if( $obj->brand_name ): ?>
                    <tr>
                        <td>бренд</td>
                        <td><a href="<?php echo Core\HTML::link('brands/'.$obj->brand_alias); ?>"><?php echo $obj->brand_name; ?></a></td>
                    </tr>
                <?php endif; ?>
                <?php if( $obj->model_name ): ?>
                    <tr>
                        <td>модель</td>
                        <td><?php echo $obj->model_name; ?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td>пол</td>
                    <td><?php echo $obj->sex == 2 ? 'Унисекс' : ( $obj->sex ? 'Женская модель' : 'Мужская модель' ); ?></td>
                </tr>
                <?php foreach ($specifications as $name => $value): ?>
                    <tr>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $value; ?></td>
                    </tr>
                <?php endforeach ?>
            </table>
        </div>
        <?php echo Core\Widgets::get('Item_InfoItemPage'); ?>
        <?php echo Core\Widgets::get('Item_QuestionAboutItem'); ?>
    </div>
</div>*/?>

<section id="product" class="pageSection pageSection--size-1">
	<div class="pageSection__inner pageSection__inner--narrow">
		<div class="pageSecion__content pageSection__content--expand">
			<div class="pageTitle _md-hide"><?php echo $obj->name;?></div>
			<div class="productCard productCard--large" data-init="dataChanger">
				<div class="productCard__info">
					<div class="productCard__title _md-show"><?php echo str_replace(' ', '<br>', $obj->name); ?></div>
					<?php if (count($images)):?>
					<?php foreach ($images as $im):?>
						<?php if ($im->main == 1):?>
							<?php $color_main = $im->color;?>
							<?php $image_main = $im->image;?>
							<?php $image_id = $im->id;?>
						<?php endif;?>
					<?php endforeach;?>
					<?php endif;?>
					<div class="productCard__featureText">
						<span class="_font-bold">Цвет: </span>
						<span class="productCard__subtitle" data-data-changer="color"><?php echo $color_main;?></span>
					</div>
					<?php $cost = ceil($obj->cost - $obj->cost*$obj->discount/100);?>
					<?php if (count($images)):?>
						<div class="productCard__thumbnailContainer">
							
							<?php foreach ($images as $im): ?>
								<?php if( is_file(HOST.Core\HTML::media('images/catalog/big/'.$im->image)) ): ?>
									<div data-data-changer="sourceImage"
										 data-info='{
												 "src":"<?php echo Core\HTML::media('images/catalog/big/'.$im->image);?>",
												 "price":<?php echo $cost;?>,
												 "oldPrice":<?php echo $obj->cost;?>,
												 "color":"<?php echo $im->color;?>",
												 "id" : <?php echo $im->id;?>}'
										 class="productCard__thumbnail <?php if ($im->main == 1) echo 'is-active';?>">
										<img src="<?php echo Core\HTML::media('images/catalog/medium/'.$im->image);?>" >
									</div>
								<?php endif;?>
							<?php endforeach;?>
						</div>
					<?php endif;?>
					<div class="productCard__priceContainer">
						<div class="productCard__priceWrap">
							<div class="productCard__featureText">Цена в фирменных<br> магазинах</div>
							<div class="productCard__price productCard__price--old"><span data-data-changer="oldPrice"><?php echo number_format($obj->cost, 0, "", "’");?></span> ₽</div>
						</div>
						<div class="productCard__priceWrap">
							<div class="productCard__featureText _font-bold">Ваша цена</div>
							<div class="productCard__price"><span data-data-changer="price"><?php echo number_format($cost, 0, "", "’");?></span> ₽</div>
						</div>
					</div>
					<button class="button button--wide button--primary button--noround mfiA" data-data-changer="button" data-url="<?php echo \Core\HTML::link('popup/orderItem'); ?>" data-param='{"id": "<?php echo $obj->id;?>", "image_id": "<?php echo $image_id;?>"}' title="Купить шаль"><span>Купить шаль</span></button>
				</div>
				<?php if (count($images)):?>
					<?php foreach ($images as $im):?>
						<?php if ($im->main == 1):?>
							<div class="productCard__imageContainer" title="Шаль Monogram Denim">	
								<?php if( is_file(HOST.Core\HTML::media('images/catalog/big/'.$im->image)) ): ?>
									<img src="<?php echo Core\HTML::media('images/catalog/big/'.$im->image);?>" data-data-changer="destinationImage">
								<?php else:?>
									<img src="<?php echo \Core\HTML::media('pic/no-image.png');?>" data-data-changer="destinationImage">
								<?php endif;?>
							</div>
						<?php endif;?>
					<?php endforeach;?>
				
				<?php endif;?>
			</div>
		</div>
	</div>
</section>

<?php echo Core\Widgets::get('Index_Gallery'); ?>	

<?php if ($obj->text OR $obj->characteristic):?>
	<section id="description" class="pageSection pageSection--size-1">
		<div class="pageSection__inner">
			<div class="pageSection__content pageSection__content--right">
				<?php echo $obj->text;?>
				<div class="wTxt">
					<?php echo $obj->characteristic;?>
				</div>
			</div>
			<?php if( is_file(HOST.Core\HTML::media('images/catalog/original/'.$image_main)) ): ?>
				<div class="pageSection__content pageSection__content--descriptionImage">
					<img src="<?php echo Core\HTML::media('images/catalog/original/'.$image_main);?>">
				</div>
			<?php endif;?>
		</div>
	</section>
<?php endif;?>

<?php echo Core\Widgets::get('Index_WhyWe'); ?>


<?php echo Core\Widgets::get('Index_Work'); ?>		

<section id="buy_item" class="pageSection pageSection--size-1">
	<div class="pageSection__inner pageSection__inner--narrow">
		<div class="pageSecion__content pageSection__content--expand">
			<div class="pageUnitTitle">Выбранная шаль</div>
			<div class="pageTitle _md-hide"><?php echo $obj->name;?></div>
			<div class="productCard productCard--large">
				<div class="productCard__info">
					<div class="productCard__title _md-show"><?php echo str_replace(' ', '<br>', $obj->name); ?></div>
					<div class="productCard__featureText"><span class="_font-bold">Цвет: </span><span class="productCard__subtitle"><?php echo $color_main;?></span></div>
					<?php if (count($images)):?>
						<div class="productCard__thumbnailContainer">
						<?php foreach ($images as $im):?>
							<?php if( is_file(HOST.Core\HTML::media('images/catalog/medium/'.$im->image)) ): ?>
								<a href="#" class="productCard__thumbnail <?php if ($im->main == 1) echo 'is-active';?>">
									<img src="<?php echo Core\HTML::media('images/catalog/medium/'.$im->image);?>">
								</a>
							<?php endif;?>
						<?php endforeach;?>
						</div>
					<?php endif;?>
					<div class="productCard__priceContainer">
						<div class="productCard__priceWrap">
							<div class="productCard__featureText">Цена в фирменных<br> магазинах</div>
							<div class="productCard__price productCard__price--old"><span><?php echo number_format($obj->cost, 0, "", "’");?></span> ₽</div>
						</div>
						<div class="productCard__priceWrap">
							<div class="productCard__featureText _font-bold">Ваша цена</div>
							<div class="productCard__price"><span><?php echo number_format($cost, 0, "", "’");?></span> ₽</div>
						</div>
					</div>
					<p>&nbsp;</p>
					<p><b>Поделиться:</b></p>
					<!--<img src="pic/images/socials.png" alt="socials"><!-- Картинка пример -->
					<!-- @htmldoc
						@todo Подключить pluso
					-->
					<!-- Скрипт pluso -->
					<script type="text/javascript">(function() {
						if (window.pluso)if (typeof window.pluso.start == "function") return;
						if (window.ifpluso==undefined) { window.ifpluso = 1;
							var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
							s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
							s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
							var h=d[g]('body')[0];
							h.appendChild(s);
						}})();
					</script>
                    <div class="pluso" data-background="transparent" data-options="big,round,line,horizontal,nocounter,theme=06" data-services="vkontakte,odnoklassniki,facebook,twitter,google"></div>
				</div>
				<?php if( is_file(HOST.Core\HTML::media('images/catalog/original/'.$image_main)) ): ?>
					<div class="productCard__imageContainer" title="Шаль Monogram Denim">
						<img src="<?php echo Core\HTML::media('images/catalog/original/'.$image_main);?>">
					</div>
				<?php endif;?>
			</div>
		</div>
	</div>
</section>
<section class="pageSection pageSection--padding">
	<div class="pageSection__inner">
		<div class="pageSection__content pageSection__content--expand">
			<div class="pageWidget" style="text-align: center;">
				<div class="pageTitle" style="margin-bottom: .5em;">Заказать шаль</div>
				<div class="wTxt" style="text-align: center; font-size: 120%;">
					<p>Наш менеджер свяжется с вами в течении 5 минут</p>
				</div>
				<div class="wForm wFormDef" data-form="true" data-ajax="OrderShawl">
					<div class="wFormRowTrio _clear-after">
						<div class="wFormRow">
							<small>Ваше имя<span class="_color-danger">*</span></small>
							<div class="wFormInput">
								<input class="wInput" type="text" data-name="name" name="order-user-name" id="order-user-name" required data-rule-word="true" data-rule-minlength="2"  placeholder="Ваше имя">
							</div>
						</div>
						<div class="wFormRow">
							<small>Ваш номер<span class="_color-danger">*</span></small>
							<div class="wFormInput">
								<input class="wInput js-inputmask" type="text" data-name="phone" name="order-user-phone" id="order-user-phone" required data-rule-pattern="\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}" data-msg-pattern="Укажите корректный номер" placeholder="+7 (___) ___-__-__">
							</div>
						</div>
						<input type="hidden" data-name="id" value="<?php echo $obj->id;?>" >
						<?php if(array_key_exists('token', $_SESSION)): ?>
							<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
						<?php endif; ?>
						<div class="wFormRow">
							<small class="show-in-norm">&nbsp;</small>
							<button class="button button--primary button--noround button--expand button--form wSubmit"><span>Заказать звонок</span></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php echo \Core\Widgets::get('Item_ItemsSame');?>