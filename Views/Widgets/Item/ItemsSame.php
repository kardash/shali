<?php if (count($result)):?>
	<section class="pageSection pageSection--background-gray pageSection--padding">
		<div class="pageSection__inner">
			<div class="pageSection__content pageSection__content--expand">
				<div class="pageUnitTitle">Ассортимент</div>
				<div class="pageTitle">Посмотрите также</div>
			</div>
			<div class="tileCards">
				<?php foreach ($result as $obj):?>
					<div class="tileCards__item">
						<div class="tileCards__itemInner productCard productCard--default">
							<?php echo Core\View::tpl(array('obj' => $obj), 'Catalog/ListItemTemplate'); ?>
						</div>
					</div>
				<?php endforeach;?>
			</div>
		</div>
	</section>
<?php endif;?>