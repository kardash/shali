<div class="pageMobileAside pageMobileAside--navigation">
	<div class="pageTitle _text-center">Навигация</div>
	<ul class="menu menu--in-pageMobileAside js-screenshift">
		
		<?php if (\Core\Route::controller() == 'index'):?>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#original" class="menu__link" title="Оригинальные шали">Оригинальные шали</a></li>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#unpacking" class="menu__link" title="Распаковка">Распаковка</a></li>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#assortment" class="menu__link" title="Асссортимент">Асссортимент</a></li>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#why_us" class="menu__link" title="Почему мы ">Почему мы</a></li>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#how_we_work" class="menu__link" title="Как мы работаем">Как мы работаем</a></li>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#reviews" class="menu__link" title="Отзывы">Отзывы</a></li>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#contacts" class="menu__link" title="Контакты">Контакты</a></li>
			<?php else:?>
		
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#product" class="menu__link <?php if (\Core\Route::param('id') == $obj->id) echo 'is-active';?>" title="<?php echo $obj->name;?>"><?php echo $obj->name;?></a></li>
				<?php if ($obj->text || $obj->characteristic):?>
					<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#description" class="menu__link" title="Оригинальные шали">Описание и характеристики</a></li>
				<?php endif;?>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#why_us" class="menu__link" title="Оригинальные шали">Почему мы</a></li>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#how_we_work" class="menu__link" title="Оригинальные шали">Как мы работаем</a></li>
				<li class="menu__item"><a data-body-toggle-class data-exception="is-mobNavigationVisible" href="#buy_item" class="menu__link" title="Купить">Купить</a></li>
			<?php endif;?>
		
	</ul>
</div>
<?php if (count($result)):?>
		<div class="pageMobileAside pageMobileAside--catalog">
			<div class="pageTitle _text-center">Каталог</div>
			<ul class="menu menu--in-pageMobileAside">
				<?php foreach ($result as $obj):?>
					<li class="menu__item">
						<a href="<?php echo \Core\HTML::link($obj->alias.'/p'.$obj->id);?>" class="menu__link" title="<?php echo $obj->name;?>"><?php echo $obj->name;?></a>
					</li>
				<?php endforeach;?>
			</ul>
		</div>
	<?php endif;?>
<div class="pageDim" data-body-toggle-class data-exception="is-mobNavigationVisible is-mobCatalogVisible"></div>
	
	
	<!-- WezomDefs -->
	<!-- script disabled message -->
	<noscript>
		<input id="wzmMsg_JsClose" type="checkbox" title="Закрыть">
		<div id="wzmMsg_JsInform" class="wzmMsg_Wrapp">
			<div class="wzmMsg_Text">
				<p>В Вашем браузере <strong>отключен JavaScript!</strong> Для корректной работы с сайтом необходима
					поддержка
					Javascript.</p>
				<p>Мы рекомендуем Вам включить использование JavaScript в настройках вашего браузера.</p>
			</div>
			<a href="http://wezom.com.ua/" target="_blank" title="Студия Wezom" class="wzmMsg_Link">
				<img src="pic/wezom-info-red.gif" width="50" height="18" alt="Студия Wezom">
			</a>
			<label for="wzmMsg_JsClose" class="wzmMsg_Close"><span>&times;</span></label>
		</div>
	</noscript>
	<!-- svg sprite -->
	<script>
		window.wSpriteSvg = function (t, e, i) {
			var n = {
				ns: "http://www.w3.org/2000/svg",
				initialize: function (e, i) {
					if (this.prefix = "wSpriteSvg_" + e.id, t.localSupport) {
						var n = t.localStorage[this.prefix];
						if (n) {
							var s = JSON.parse(n);
							this.setSprite(e, s)
						} else this.getJson(e, i)
					} else this.getJson(e, i)
				},
				getJson: function (e, i) {
					var n = new XMLHttpRequest,
							s = this;
					n.open("GET", i, !0), n.setRequestHeader("Content-type", "application/json"), n.onreadystatechange = function () {
						if (4 == n.readyState && 200 == n.status) {
							var i = JSON.parse(n.responseText);
							s.setSprite(e, i), t.localSupport && t.localWrite(s.prefix, JSON.stringify(i))
						}
					}, n.send()
				},
				buildElem: function (t, i) {
					var n, s, r;
					for (n in i)
						for (s in i[n]) {
							var o = e.createElementNS(this.ns, s);
							for (r in i[n][s]) "stops" === r ? this.buildElem(o, i[n][s][r]) : o.setAttributeNS(null, r, i[n][s][r]);
							t.appendChild(o)
						}
				},
				setSprite: function (t, i) {
					for (var n in i) {
						var s = i[n];
						console.log();
						var r = e.createElementNS(this.ns, "symbol");
						r.setAttributeNS(null, "id", n), r.setAttributeNS(null, "viewBox", s.viewBox), this.buildElem(r, s.symbol), s.hasOwnProperty("gradients") && this.buildElem(t, s.gradients), t.appendChild(r)
					}
					this.isDone(t, i)
				},
				isDone: function (t, e) {
				}
			};
			return n
		}(this, this.document);
	</script>
	<!-- old browser detect -->
	<script>
		function $wzmOldInit() {
			var scrpt = document.createElement("script");
			scrpt.src = "http://verstka.vps.kherson.ua/sources/plugins/wold/wold.js";
			document.body.appendChild(scrpt);
		}

		try {
			document.addEventListener("DOMContentLoaded", $wzmOldInit, false);
		} catch (e) {
			window.attachEvent("onload", $wzmOldInit);
		}
	</script>
	<!-- изменить id у svg элемента -> #sprite_SiteName -->
	<svg id="sprite-louis-vouitton" xmlns="http://www.w3.org/2000/svg"
		 style="height:0; width:0; visibility:hidden; position:absolute; top:0; left:0;"
		 onload="wSpriteSvg.initialize(this,'<?php echo \Core\HTML::media("js/jsons/svgsprite.json");?>')"></svg>

	<!-- Внешние js файлы -->
	<script src="/Media/js/jquery-2.2.4.min.js"></script>
	<script src="/Media/js/components.js"></script>
	<script src="/Media/js/libs.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyqZU3EGbB02sCB-O0JXmYlqcnYlEvAXk">
	</script>
	<script src="/Media/js/init.js"></script>
	<script src="/Media/js/validation.js"></script>