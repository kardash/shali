<header class="pageHeader">
	<div class="pageHeader__inner pageSize">
		<div class="pageHeader__item">
			<a href="<?php echo \Core\HTML::link();?>" class="logo logo--in-pageHeader">
				<div class="logo__main">
					<img src="<?php echo \Core\HTML::media('pic/logo-small.png');?>" alt="">
				</div>
				<div class="logo__secondary">
					<img src="<?php echo \Core\HTML::media('pic/logo-small-txt.png');?>" alt="">
				</div>
			</a>
		</div>
		<div class="pageHeader__item _md-show">
			<?php if (count($items)):?>
				<ul class="menu menu--secondary-in-pageHeader menu--large menu--bordered _text-center">
					<?php foreach ($items as $item):?>
						<li class="menu__item">
							<a href="<?php echo \Core\HTML::link($item->alias.'/p'.$item->id);?>" class="menu__link" title="<?php echo $item->model;?>"><?php echo $item->model;?></a>
						</li>
					<?php endforeach;?>
				</ul>
			<?php endif;?>
			
			
			<?php if (\Core\Route::controller() == 'index'):?>
				<ul class="menu menu--main-in-pageHeader menu--buttoned _text-center js-screenshift">
					<li class="menu__item"><a href="#original" class="menu__link" title="Оригинальные шали">Оригинальные шали</a></li>
					<li class="menu__item"><a href="#unpacking" class="menu__link" title="Распаковка">Распаковка</a></li>
					<li class="menu__item"><a href="#assortment" class="menu__link" title="Асссортимент">Асссортимент</a></li>
					<li class="menu__item"><a href="#why_us" class="menu__link" title="Почему мы ">Почему мы</a></li>
					<li class="menu__item"><a href="#how_we_work" class="menu__link" title="Как мы работаем">Как мы работаем</a></li>
					<li class="menu__item"><a href="#reviews" class="menu__link" title="Отзывы">Отзывы</a></li>
					<li class="menu__item"><a href="#contacts" class="menu__link" title="Контакты">Контакты</a></li>
				</ul>
			<?php else:?>
				<ul class="menu menu--main-in-pageHeader menu--buttoned _text-center js-screenshift">
					
					<li class="menu__item"><a href="#product" class="menu__link <?php if (\Core\Route::param('id') == $obj->id) echo 'is-active';?>" title="<?php echo $obj->name;?>"><?php echo $obj->name;?></a></li>
					<?php if ($obj->text || $obj->characterictic):?>
						<li class="menu__item"><a href="#description" class="menu__link" title="Оригинальные шали">Описание и характеристики</a></li>
					<?php endif;?>
					<li class="menu__item"><a href="#why_us" class="menu__link" title="Оригинальные шали">Почему мы</a></li>
					<li class="menu__item"><a href="#how_we_work" class="menu__link" title="Оригинальные шали">Как мы работаем</a></li>
					<li class="menu__item"><a href="#buy_item" class="menu__link" title="Купить">Купить</a></li>
					
				</ul>
			<?php endif;?>
		</div>
		<div class="pageHeader__item pageHeader__item--buttonWrap">
			<button class="button mfiA" data-url="<?php echo \Core\HTML::link('popup/callback'); ?>" title="Заказать звонок"><span>Заказать звонок</span></button>
		</div>
		<div class="pageHeader__item pageHeader__item--menuButtons _md-hide">
			<button class="roundButton" data-body-toggle-class="is-mobNavigationVisible" data-exception="is-mobCatalogVisible">
				<svg>
					<use xlink:href="#icon_burger"></use>
				</svg>
			</button>
			<button class="roundButton" data-body-toggle-class="is-mobCatalogVisible" data-exception="is-mobNavigationVisible">
				<svg>
					<use xlink:href="#icon_list_right"></use>
				</svg>
			</button>
		</div>
	</div>
</header>