<section id="why_us" class="pageSection pageSection--size-2 pageSection--background-dark pageSection--decor-4 js-screen">
	<div class="pageSection__inner">
		<div class="pageSection__content pageSection__content--right">
			<?php echo \Core\Config::get('static.why_we_text');?>
		</div>
	</div>
</section>