<section id="original" class="pageSection pageSection--size-1 pageSection--decor-1 pageSection--background-gray">
	<div class="pageSection__inner">
		<div class="pageSection__content pageSection__content--right">
			<?php echo \Core\Config::get('static.original_text');?>
		</div>
	</div>
</section>