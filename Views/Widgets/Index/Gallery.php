<section id="gallery" class="pageSection">
	<div class="tilePictures">
		<div class="tilePictures__item tilePictures__item--doubleHeight" style="background-image: url(<?php echo \Core\HTML::media('pic/images/img-tile-1.jpg');?>)"></div>
		<div class="tilePictures__item tilePictures__item--texted" style="background-image: url(<?php echo \Core\HTML::media('pic/images/img-tile-text.png');?>)">
			<div class="tilePictures__text">
				МНОГИЕ ЗНАМЕНИТОСТИ <br>
				ВЫБРАЛИ ДЛЯ СЕБЯ ШАЛИ ОТ <br>
				LOUIS VUITTON
			</div>
		</div>
		<div class="tilePictures__item tilePictures__item--doubleWidth" style="background-image: url(<?php echo \Core\HTML::media('pic/images/img-tile-2.jpg');?>)"></div>
		<div class="tilePictures__item" style="background-image: url(<?php echo \Core\HTML::media('pic/images/img-tile-3.jpg');?>)"></div>
		<div class="tilePictures__item" style="background-image: url(<?php echo \Core\HTML::media('pic/images/img-tile-4.jpg');?>)"></div>
		<div class="tilePictures__item" style="background-image: url(<?php echo \Core\HTML::media('pic/images/img-tile-5.jpg');?>)"></div>
		<div class="tilePictures__item" style="background-image: url(<?php echo \Core\HTML::media('pic/images/img-tile-6.jpg');?>)"></div>
		<div class="tilePictures__item" style="background-image: url(<?php echo \Core\HTML::media('pic/images/img-tile-7.jpg');?>)"></div>
	</div>
</section>