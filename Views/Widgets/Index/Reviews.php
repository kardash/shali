<?php if (count($result)):?>
<section id="reviews" class="pageSection pageSection--padding pageSection--background-gray">
	<div class="pageSection__inner">
		<div class="pageSection__content pageSection__content--expand">
			<div class="pageUnitTitle _text-center">Отзывы</div>
			<div class="pageTitle _text-center">покупатели говорят</div>
			<div class="carousel carousel--review">
				<div class="carousel__track js-carousel" data-preset="main">
					<?php foreach($result as $obj):?>
						<div class="carousel__slide">
							<div class="carousel__slideInner">
								<div class="carousel__reviewText">
									<p><?php echo $obj->text;?></p>
								</div>
								<?php if( is_file(HOST.Core\HTML::media('images/reviews/small/'.$obj->image)) ): ?>
									<div class="carousel__reviewAvatar" style="background-image: url(<?php echo \Core\HTML::media('images/reviews/small/'.$obj->image);?>)"></div>
								<?php endif;?>
								<div class="carousel__caption">- <?php echo $obj->name.', '.$obj->company;?></div>
							</div>
						</div>
					<?php endforeach;?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;?>