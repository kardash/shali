<section id="contacts" data-init="tabbedMap">
	<div class="pageSection pageSection--padding pageSection--background-silver pageSection--decor-6">
		<div class="pageSection__inner">
			<div class="pageSection__content pageSection__content--left pageSection__content--expand">
				<div class="pageUnitTitle _color-white">Почему мы</div>
				<div class="pageTitle _color-white">
					закажите шаль сейчас<br>
					или приходите в наши<br>
					шоурумы в городах</div>
				<?php if (count($result)): $i=0;?>
					<div data-tabbedmap="controls">
						<?php foreach ($result as $obj):?>
							<?php $data = json_decode($obj->map, TRUE); $i=$i+1;?>
								<button class="button" data-infowindow="infoWindow<?php echo $i;?>" data-lat="<?php echo \Core\Arr::get($data, 'latitude'); ?>" data-lng="<?php echo \Core\Arr::get($data, 'longitude'); ?>" data-tabbedmap="initial"><?php echo $obj->city;?></button>
						<?php endforeach;?>
					</div>
				<?php endif;?>
			</div>
		</div>
	</div>
	<div data-tabbedmap="map" class="gmap"></div>
	<div class="pageWidget" style="text-align: center;">
		<div class="mfiModal mfiModal--form mfiModal--styled mfiModal--black">
			<div class="pageTitle" style="margin-bottom: .5em;">Заказать шаль</div>
			<div class="wTxt" style="text-align: left; font-size: 120%;">
				<p>Наш менеджер свяжется с вами в течении 5 минут</p>
			</div>
			<div class="wForm wFormDef" data-form="true" data-ajax="orderShawl">
				<div class="wFormRowDouble _clear-after">
					<div class="wFormRow">
						<small>Имя<span class="_color-danger">*</span></small>
						<div class="wFormInput">
							<input class="wInput" type="text" data-name="name" name="callback-user-name" required data-rule-word="true" data-rule-minlength="2">
						</div>
					</div>
					<div class="wFormRow">
						<small>Телефон<span class="_color-danger">*</span></small>
						<div class="wFormInput">
							<input class="wInput js-inputmask" data-name="phone"  type="text" name="callback-user-phone" id="callback-user-phone" required data-rule-pattern="\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}" data-msg-pattern="Укажите корректный номер" placeholder="+7 (___) ___-__-__">
						</div>
					</div>
				</div>
				<?php if(array_key_exists('token', $_SESSION)): ?>
					<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
				<?php endif; ?>
				<div class="wFormRow">
					<!--<small class="show-in-norm">&nbsp;</small>-->
					<button class="button button--primary button--noround button--in-map wSubmit"><span>Заказать шаль</span></button>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if (count($result)): $j=0;?>
<div class="_hide">
	<?php foreach ($result as $obj): $j=$j+1;?>
		<div id="infoWindow<?php echo $j;?>" class="infoWindow">
			<div class="infoWindow__image">
				<img src="<?php echo \Core\HTML::media('pic/images/map-img.png');?>" alt="">
			</div>
			<div class="infoWindow__content">
				<div class="infoWindow__title">г. <?php echo $obj->city;?>,<br><?php echo $obj->street;?></div>
				<div class="infoWindow__phone">
					тел.<a href="tel:<?php echo preg_replace('/[^0-9]/', '',$obj->phone);?>" class=""> <?php echo $obj->phone;?></a>
				</div>
				<button class="infoWindow__button button mfiA" data-url="<?php echo \Core\HTML::link('popup/callback'); ?>" data-param="{}">
					Заказать звонок
				</button>
			</div>
		</div>
	<?php endforeach;?>
</div>
<?php endif;?>