<section id="unpacking" class="pageSection pageSection--size-1 pageSection--decor-2 pageSection--background-dark ">
	<div class="pageSection__inner">
		<div class="pageSection__content pageSection__content--left pageSection__content--space-bottom">
			<div class="pageUnitTitle _color-white">Распаковка</div>
			<div class="pageTitle _color-white">Получение и распаковка</div>
			<div class="pageDescription _color-gray">Мы даем гарантию, что наши шали 100% оригинальные. Посмотрите фото и видео распаковки товара</div>
			<a class="mediaButton mfiF" href="<?php echo \Core\Config::get('static.video');?>" title="Видео распаковки"><i><svg><use xlink:href="#icon_play"></use></svg></i></a>
			<p class="_font-secondary _color-white" style="font-size: 1.25rem;">ВИДЕО РАСПАКОВКИ</p>
		</div>
		<?php if (count($result)):?>
			<div class="pageSection__content pageSection__content--carouselContainer">
				<div class="carousel carousel--round">
					<div class="carousel__main js-carousel" data-preset="round_main">
						<?php foreach ($result as $obj): ?>
							<?php if( is_file(HOST.Core\HTML::media('images/gallery_images/small/'.$obj->image)) ): ?>
								<div class="carousel__slide" style="background-image: url(<?php echo Core\HTML::media('images/gallery_images/small/'.$obj->image);?>)"></div>
							<?php endif;?>
						<?php endforeach;?>
					</div>
					<div class="carousel__track js-carousel" data-preset="round_navigation">
						<?php foreach ($result as $obj):?>
							<?php if( is_file(HOST.Core\HTML::media('images/gallery_images/small/'.$obj->image)) ): ?>
								<div class="carousel__slide carousel__slide--decorIfActive" style="background-image: url(<?php echo Core\HTML::media('images/gallery_images/small/'.$obj->image);?>)"></div>
							<?php endif;?>
						<?php endforeach;?>
					</div>
				</div>
			</div>
		<?php endif;?>
	</div>
</section>