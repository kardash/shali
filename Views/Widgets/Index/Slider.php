<?php if (count($result)):?>
	<section class="pageSection">
		<div class="carousel carousel--index">
			<div class="carousel__track js-carousel" data-preset="main">
				<?php foreach ($result as $obj):?>
					<?php if( is_file(HOST.Core\HTML::media('images/slider/big/'.$obj->image)) ): ?>
						<div class="carousel__slide" style="background-image: url(<?php echo \Core\HTML::media('images/slider/big/'.$obj->image);?>);">
							<div class="carousel__slideInner">
								<div class="carousel__logoHolder">
									<img src="<?php echo \Core\HTML::media('pic/logo-normal.png');?>" alt="Louis Vuitton">
								</div>
								<div class="carousel__text">
									<?php echo $obj->name;?>
								</div>
								<div class="_text-center">
									<button class="button button--in-carousel button--large button--hollow-white button--noround mfiA" data-url="hidden/order.php" data-param='{"id": 378}' title="Заказать шаль"><span>Заказать шаль</span></button>
								</div>
							</div>
						</div>
					<?php endif;?>
				<?php endforeach;?>
			</div>
			<div class="js-screenshift">
				<a href="#gallery" class="iconButton iconButton--in-carouselWrap _xl-show"><svg><use xlink:href="#icon_double_chevron_down"></use></svg></a>
			</div>
		</div>
	</section>
<?php endif;?>