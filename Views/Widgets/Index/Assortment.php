<?php if (count($result)):?>
	<section id="assortment" class="pageSection pageSection--size-1 pageSection--decor-3 pageSection--background-gray">
		<div class="pageSection__inner pageSection__inner--wide">
			<div class="pageSection__content pageSection__content--expand">
				<div class="pageUnitTitle">Ассортимент</div>
				<div class="pageTitle">Все шали в наличии</div>
			</div>
			<div class="tileCards">
				<?php foreach ($result as $obj):?>
					<div class="tileCards__item" data-init="dataChanger">
						<div class="tileCards__itemInner productCard productCard--default">
							<?php echo Core\View::tpl(array('obj' => $obj), 'Catalog/ListItemTemplate'); ?>
						</div>
					</div>
				<?php endforeach;?>				
			</div>
		</div>
	</section>
<?php endif;?>