<section id="how_we_work" class="pageSection pageSection--size-1 pageSection--background-primary pageSection--decor-5 js-screen">
	<div class="pageSection__inner">
		<?php echo \Core\Config::get('static.we_work_text');?>
	</div>
</section>