<div class="pageFooter">
	<div class="pageFooter__top">
		<div class="footerLogo">
			<img class="footerLogo__logo" src="<?php echo \Core\HTML::media('pic/logo-normal.png');?>" alt="">
			<img class="footerLogo__customText" src="<?php echo \Core\HTML::media('pic/original-shal.svg');?>" alt="">
			<div class="footerLogo__text">louis vuitton</div>
		</div>
	</div>
	<div class="pageFooter__bottom">
		<div class="copyrights"><?php echo \Core\Config::get('static.copyright');?></div>
		<div class="wezomLogo"><a href="wezom.com.ua" target="_blank">Студия  wezom<svg><use xlink:href="#icon_wezom"></use></svg></a></div>
	</div>
</div>