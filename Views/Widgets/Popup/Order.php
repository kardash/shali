<div class="mfiModal zoomAnim orderPopup">
	<div class="h2"><?php echo __('Заказ индивидуального трансфера');?></div>

	<div data-form="true" data-ajax="ind_order" class="wForm wFormDef">
		<div class="clearfix order-popup-top">
			<div class="wFormRow">
				<div class="wLabel"><?php echo __('ФИО');?></div>
				<div class="wFormInput">
					<input class="wInput" type="text" data-name="fio" name="name" data-rule-minlength="2" data-rule-word="true" >
				</div>
			</div>

			<div class="wFormRow">
				<div class="wLabel">E-mail</div>
				<div class="wFormInput">
					<input class="wInput" required="" data-name="email" type="text" name="mail" data-rule-email="true" aria-required="true">
				</div>
			</div>

			<div class="wFormRow">
				<div class="wLabel"><?php echo __('Страна');?></div>
				<div class="wFormInput">
					<input class="wInput" type="text" data-name="country" name="country" data-rule-minlength="3">
				</div>
			</div>

			<div class="wFormRow">
				<div class="wLabel"><?php echo __('Номер телефона');?></div>
				<div class="wFormInput">
					<input class="wInput phone-mask" type="text" data-name="phone" name="phone" data-rule-phone="true">
				</div>
			</div>

			<div class="wFormRow">
				<div class="wLabel"><?php echo __('Пассажиров');?></div>
				<div class="wFormInput">
					<input class="wInput" required="" type="text" data-name="passengers" name="count" data-rule-number="true" aria-required="true">
				</div>
			</div>
		</div>


		<div class="clearfix order-popup-bottom">
			<div class="order-popup-bottom_col">
				<div class="h2"><?php echo __('Отправление');?></div>

				<div class="order-popup-date_row clearfix">
					<div class="order-popup-date_col">
						<div class="wFormRow">
							<div class="wFormInput">
								<input class="wInput datepicker" required="" data-name="departure_date" type="text" name="date-from" placeholder="<?php echo __('Дата отправления');?>" data-rule-minlength="2" aria-required="true">
							</div>
						</div>
					</div>

					<div class="order-popup-date_col">
						<div class="wFormRow">
							<div class="wFormInput">
								<input class="wInput timepicker" required="" data-name="departure_time" type="text" name="time-from" placeholder="<?php echo __('Время отправления');?>" data-rule-minlength="2" aria-required="true">
							</div>
						</div>
					</div>
				</div>

				<div class="wFormRow">
					<div class="wFormInput">
						<input class="wInput " required="" type="text" name="location-from" data-name="departure_address" placeholder="<?php echo __('Адрес точки отправления');?>" data-rule-minlength="2" aria-required="true">
						<div class="inpInfo"><?php echo __('Адрес отправления');?></div>
					</div>
				</div>

				<div class="wFormRow">
					<div class="wFormInput">
						<textarea class="wTextarea"  name="msg-from" data-name="departure_info" placeholder="<?php echo __('Дополнительная информация');?>" data-rule-minlength="10" aria-required="true"></textarea>
						<div class="inpInfo"><?php echo __('Дополнительная информация');?></div>
					</div>
				</div>
			</div>

			<div class="order-popup-bottom_col">
				<div class="h2"><?php echo __('Возврат');?></div>
				
				<div class="order-popup-date_row clearfix">
					<div class="order-popup-date_col">
						<div class="wFormRow">
							<div class="wFormInput">
								<input class="wInput datepicker" required="" data-name="return_date" type="text" name="date-to" placeholder="<?php echo __('Дата возврата');?>" data-rule-minlength="2" aria-required="true">
							</div>
						</div>
					</div>

					<div class="order-popup-date_col">
						<div class="wFormRow">
							<div class="wFormInput">
								<input class="wInput timepicker" required="" data-name="return_time" type="text" name="time-to" placeholder="<?php echo __('Время возврата');?>" data-rule-minlength="2" aria-required="true">
							</div>
						</div>
					</div>
				</div>

				<div class="wFormRow">
					<div class="wFormInput">
						<input class="wInput " required="" type="text" data-name="return_address" name="location-to" placeholder="<?php echo __('Адрес точки возврата');?>" data-rule-minlength="2" aria-required="true">
						<div class="inpInfo"><?php echo __('Адрес отправления');?></div>
					</div>
				</div>

				<div class="wFormRow">
					<div class="wFormInput">
						<textarea class="wTextarea" name="msg-to" data-name="return_info" placeholder="<?php echo __('Дополнительная информация');?>" data-rule-minlength="10" aria-required="true"></textarea>
						<div class="inpInfo"><?php echo __('Дополнительная информация');?></div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" data-name="lang" value="<?php echo \I18n::lang();?>">
		<input type="hidden" data-name="catalog_id" value="<?php echo $item_id;?>">
		<input type="hidden" data-name="parent_id" value="<?php echo $parent_id;?>">
		<input type="hidden" data-name="route_id" value="<?php echo $route_id;?>">
		<?php if(array_key_exists('token', $_SESSION)): ?>
			<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
		<?php endif; ?>


		<div class="wFormRow w_last m-tac">
			<button class="wSubmit button"><?php echo __('Заказать трансфер');?></button>
		</div>
	</div>
</div>