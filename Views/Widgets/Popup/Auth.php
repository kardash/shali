<div class="mfiModal big zoomAnim">
    <script>
        jQuery(document).ready(function($) {
            $('#forget_pass').on('click', function(event) {
                $('#entrForm').removeClass('visForm');
                $('#forgetForm').addClass('visForm');
            });
            $('#remember_pass').on('click', function(event) {
                $('#forgetForm').removeClass('visForm');
                $('#entrForm').addClass('visForm');
            });
            $('#enterReg').on('click', '.erTitle', function(event) {
                event.preventDefault();
                if($(window).width() < 720) {
                    if(!$(this).parent().hasClass('wCur')) {
                        $('#enterReg .popupBlock').removeClass('wCur').filter($(this).parent()).addClass('wCur');
                    }
                }
            });
        });
    </script>
    <div class="popup-header">
        <div class="popup-header__img-holder">
            <img src="<?php echo \Core\HTML::media('pic/logo--shtamp_'.\I18n::$lang.'.png'); ?>" alt="">
        </div>
        <div class="popup-header__title">
            <img src="<?php echo \Core\HTML::media('pic/popup-heading_'.\I18n::$lang.'.png'); ?>" alt="">
            <p><?php echo __('Вход в наш интернет магазин'); ?></p>
        </div>
    </div>
    <div id="enterReg" class="popup-body">
        <div class="enterReg_top">
            <div class="popupBlock enterBlock wCur">
                <div class="erTitle"><?php echo __('Вход на сайт'); ?></div>
                <div class="popupContent">
                    <div id="entrForm" data-form="true" class="wForm wFormDef enterBlock_form visForm" data-ajax="login">
                        <div class="wFormRow">
                            <input data-name="email" class="wInput" type="email" name="enter_email" data-msg-required="<?php echo __('Это поле необходимо заполнить'); ?>" data-msg-email="<?php echo __('Пожалуйста, введите корректный Email'); ?>" placeholder="E-mail" required="">
                            <div class="inpInfo">E-mail</div>
                        </div>
                        <div class="wFormRow">
                            <input data-name="password" class="wInput" type="password" name="enter_pass" data-msg-required="<?php echo __('Это поле необходимо заполнить'); ?>" data-msg-minlength="<?php echo __('Пожалуйста, введите не меньше 4 символов'); ?>" data-rule-minlength="4" placeholder="<?php echo __('Пароль'); ?>" required="">
                            <div class="inpInfo"><?php echo __('Пароль'); ?></div>
                        </div>
                        <label class="checkBlock">
                            <input type="checkbox" checked="checked" value="1" data-name="remember">
                            <ins></ins>
                            <span><?php echo __('Запомнить данные'); ?></span>
                        </label>
                        <?php if(array_key_exists('token', $_SESSION)): ?>
                            <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                        <?php endif; ?>
                        <div class="tac">
                            <button class="wSubmit wBtn xlg primary"><?php echo __('Войти'); ?></button>
                        </div>
                        <div class="tac">
                            <span class="passLink" id="forget_pass"><?php echo __('Забыли пароль?'); ?></span>
                        </div>
                    </div>
                    <div id="forgetForm" data-form="true" class="wForm wFormDef enterBlock_form" data-ajax="forgot_password">
                        <div class="wFormRow">
                            <input data-name="email" class="wInput" type="email" name="forget_email" data-msg-required="<?php echo __('Это поле необходимо заполнить'); ?>" data-msg-email="<?php echo __('Пожалуйста, введите корректный Email'); ?>" placeholder="E-mail" required="">
                            <div class="inpInfo">E-mail</div>
                        </div>
                        <?php if(array_key_exists('token', $_SESSION)): ?>
                            <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                        <?php endif; ?>
                        <div class="forgetInf">
                            <?php echo __('Сообщение для восстановления пароля'); ?>
                        </div>
                        <div class="passLink" id="remember_pass"><?php echo __('Вернуться'); ?></div>
                        <div class="tar">
                            <button class="wSubmit wBtn xlg primary"><?php echo __('Отправить'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <div data-form="true" class="popupBlock wForm wFormDef regBlock" data-ajax="registration">
                <div class="erTitle"><?php echo __('Новый покупатель'); ?></div>
                <div class="popupContent">
                    <div class="wFormRow">
                        <input data-name="email" class="wInput" type="email" name="reg_email" data-msg-required="<?php echo __('Это поле необходимо заполнить'); ?>" data-msg-email="<?php echo __('Пожалуйста, введите корректный Email'); ?>" placeholder="E-mail" required="">
                        <div class="inpInfo">E-mail</div>
                    </div>
                    <div class="wFormRow">
                        <input data-name="password" class="wInput" type="password" name="reg_pass" data-msg-required="<?php echo __('Это поле необходимо заполнить'); ?>" data-msg-minlength="<?php echo __('Пожалуйста, введите не меньше 4 символов'); ?>" data-rule-minlength="4" placeholder="<?php echo __('Пароль'); ?>" required="">
                        <div class="inpInfo"><?php echo __('Пароль'); ?></div>
                    </div>
                    <label class="checkBlock">
                        <input data-name="agree" value="1" type="checkbox" name="reg_agree" data-msg-required="<?php echo __('Это поле нужно отметить'); ?>" required="">
                        <ins></ins>
                        <span><?php echo __('Принимаю условия использования и обработки моих персональных данных'); ?></span>
                    </label>
                    <?php if(array_key_exists('token', $_SESSION)): ?>
                        <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                    <?php endif; ?>
                    <div class="tac">
                        <button class="wSubmit wBtn xlg primary"><?php echo __('Зарегистрироваться'); ?></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="popupBlock socEnter">
            <div class="erTitle"><?php echo __('Вход через соц. сети'); ?></div>
            <div class="popupContent socLinkEnter">
                <div data-ulogin="display=small;fields=first_name,last_name,email;providers=vkontakte,odnoklassniki,instagram,facebook;hidden=;redirect_uri=http%3A%2F%2F<?php echo $_SERVER['HTTP_HOST']; ?>%2Faccount%2Flogin-by-social-network" id="uLogin"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>