<div data-form="true" class="testlesson cloudForm__block wForm wFormDef" data-ajax="trial_lesson">
	<div class="cloudForm__title"><?php echo __('Запишитесь прямо сейчас на бесплатное пробное занятие');?></div>
	<div class="cloudForm__body">
		<div class="wFormRow">
			<div class="wFormInput">
				<input required="required" data-name="name" type="text" placeholder="<?php echo __('Введите свое имя');?>" data-rule-word="true" data-rule-minlength='2' name="name" class="wInput"/>
				<div class="inpInfo"><?php echo __('Имя');?></div>
			</div>
		</div>
		<div class="wFormRow">
			<div class="wFormInput">
				<input required="required" type="tel" data-name="phone" placeholder="<?php echo __('Введите свой телефон');?>" data-rule-phone="true" name="tel" class="wInput"/>
				<div class="inpInfo"><?php echo __('Телефон');?></div>
			</div>
		</div>
		<div class="wFormRow">
			<select data-placeholder="<?php echo __('Выберите филиал');?>" data-name="branch_id" class="wSelect">
				<option selected="selected" disabled="disabled"><?php echo __('Выберите филиал');?></option>
				<?php if (count($branches)):?>
					<?php foreach ($branches as $branch):?>
						<option value="<?php echo $branch->id;?>"><?php echo $branch->name;?></option>
					<?php endforeach;?>
				<?php endif;?>
			</select>
		</div>
		<div class="wFormRow">
			<select data-placeholder="<?php echo __('Выберите возрастную группу');?>" data-name="group_id" ="wSelect">
				<option selected="selected" disabled="disabled"><?php echo __('Выберите возрастную группу');?></option>
				<?php if (count($groups)):?>
					<?php foreach ($groups as $group):?>
						<option value="<?php echo $group->id;?>"><?php echo $group->name;?></option>
					<?php endforeach;?>
				<?php endif;?>
			</select>
		</div>
		<input type="hidden" data-name="lang" value="<?php echo \I18n::lang();?>">
		<?php if(array_key_exists('token', $_SESSION)): ?>
             <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
        <?php endif; ?>
		<div class="wFormRow">
			<button class="wBtn wSubmit"><?php echo __('Отправить заявку прямо сейчас');?></button>
		</div>
	</div>
</div>