<div class="mfiModal mfiModal--form mfiModal--preview">
	<div class="mfiModal__left">
		<div class="wTxt">
			<p><strong>Ваш выбор:</strong></p>
			<p style="font-size: 110%;"><?php echo $item->name;?></p>
			<p style="color: #787878">
				Цвет: <?php echo $item->color;?></br>
				Ваша цена: <?php echo number_format($cost, 0, "", "’");?> Р
			</p>
			<?php if( is_file(HOST.Core\HTML::media('images/catalog/medium/'.$item->image)) ): ?>
			<p style="text-align: center;">
				<img src="<?php echo Core\HTML::media('images/catalog/medium/'.$item->image);?>" alt>
			</p>
			<?php endif;?>
		</div>
	</div>
	<div class="mfiModal mfiModal--form mfiModal--styled">
		<button title="Закрыть (ESC)" type="button" class="mfp-close">&times;</button>
		<div class="pageTitle">Заказать шаль</div>
		<div class="wTxt">
			<p>Наш менеджер свяжется с вами в течении 5 минут</p>
		</div>
		<div class="wForm wFormDef" data-form="true" data-ajax="OrderShawl">
			<div class="wFormRowDouble _clear-after">
				<div class="wFormRow">
					<small>Ваше имя<span class="_color-danger">*</span></small>
					<div class="wFormInput">
						<input class="wInput" type="text" name="order-user-name" data-name="name" id="order-user-name" required data-rule-word="true" data-rule-minlength="2"  placeholder="Ваше имя">
					</div>
				</div>
				<div class="wFormRow">
					<small>Ваше номер<span class="_color-danger">*</span></small>
					<div class="wFormInput">
						<input class="wInput js-inputmask" type="text" name="order-user-phone" data-name="phone" id="order-user-phone" required data-rule-pattern="\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}" data-msg-pattern="Укажите корректный номер" placeholder="+7 (___) ___-__-__">
					</div>
				</div>
			</div>
			<input type="hidden" data-name="catalog_id" value="<?php echo $item->id;?>">
			<input type="hidden" data-name="color" value="<?php echo $item->color;?>">
			<?php if(array_key_exists('token', $_SESSION)): ?>
				<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
			<?php endif; ?>
			<div class="wFormRow" style="max-width: 310px;">
				<button class="button button--primary button--noround button--expand wSubmit"><span>Заказать оригинальную шаль</span></button>
			</div>
		</div>
	</div>
</div>
