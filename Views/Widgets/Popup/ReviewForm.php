<div class="feedback-form">
	<div class="h1"><?php echo __('добавить отзыв');?></div>
	<div data-form="true" data-ajax="reviews" class="wForm wFormDef">
		<div class="wFormRow">
			<div class="wFormInput">
				<input class="wInput" required type="text" data-name="name" name="name" placeholder="<?php echo __('Введите Ваше имя');?>" data-rule-minlength="2" data-rule-word="true" aria-required="true">
				<div class="inpInfo"><?php echo __('Ваше имя');?></div>
			</div>
		</div>
		<div class="wFormRow">
			<div class="wFormInput">
				<input class="wInput email-mask" required type="text" data-name="email" name="mail" placeholder="<?php echo __('Введите Ваш e-mail');?>" data-rule-email="true" aria-required="true">
				<div class="inpInfo"><?php echo __('Ваш e-mail');?></div>
			</div>
		</div>
		<div class="wFormRow rating">
			<div class="wLabel"><?php echo __('оцените нашу работу');?></div>
			<span class="rating__stars">
				<input type="radio" name="rating" value="10" required aria-required="true">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
				<input type="radio" name="rating" value="9">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
				<input type="radio" name="rating" value="8">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
				<input type="radio" name="rating" value="7">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
				<input type="radio" name="rating" value="6">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
				<input type="radio" name="rating" value="5">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
				<input type="radio" name="rating" value="4">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
				<input type="radio" name="rating" value="3">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
				<input type="radio" name="rating" value="2">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
				<input type="radio" name="rating" value="1">
				<i>
					<svg><use xlink:href="#icon_star"/></svg>
				</i>
			</span>
			<input type="hidden" data-name="mark" name="mark" value="">
		</div>
		<div class="feedback-form_bottom">
			<div class="feedback-form_row">
				<div class="feedback-form_col">
					<div class="wFormRow">
						<div class="wFormInput">
							<input class="wInput datepicker-feedback" required type="text" data-name="date_travel" name="time" placeholder="<?php echo __('Дата поездки');?>" data-rule-minlength="2" aria-required="true">
						</div>
					</div>
				</div>
				<div class="feedback-form_col">
					<div class="wFormRow">
						<div class="wFormInput">
							<select class="wSelect" required  data-name="group" name="demo_required_select" id="demo_required_select" aria-required="true">
								<option value="" selected disabled><?php echo __('Выберите услугу');?></option>
								<option value="8"><?php echo __('Трансферы');?></option>
								<?php if (count($groups)):?>
									<?php foreach($groups as $obj):?>
										<option value="<?php echo $obj->id;?>"><?php echo $obj->name;?></option>
									<?php endforeach;?>
								<?php endif;?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="wFormRow">
				<div class="wFormInput">
					<textarea class="wTextarea" required name="msg" data-name="text" placeholder="" data-rule-minlength="10" aria-required="true"></textarea>
					<div class="feedback-form_textarea-placeholder"><?php echo __('Вы можете написать');?>
						<br>1. <?php echo __('Почему выбрали именно нашу компанию');?> 
						<br>2. <?php echo __('Понравилась ли вам работа менеджера до/после покупки');?> 
						<br>3. <?php echo __('Обратитесь ли вы к нам еще раз');?>
					</div>
					<div class="inpInfo"><?php echo __('Сообщение');?></div>
				</div>
			</div>
			<input type="hidden" data-name="lang" value="<?php echo \I18n::lang();?>">
			<?php if(array_key_exists('token', $_SESSION)): ?>
				<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
			<?php endif; ?>
			<div class="wFormRow w_last m-tac">
				<button class="wSubmit button"><?php echo __('добавить отзыв');?></button>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	$('input[name="rating"]').on('click', function() {
		$('input[name="mark"]').val($(this).val());
	} );
} );
</script>
