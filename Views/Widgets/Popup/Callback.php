<?php /*<div class="mfiModal zoomAnim">
	<div class="h2"><?php echo __('Заказать звонок');?></div>
	<div data-form="true" data-ajax="callback" class="wForm wFormDef">
		<div class="wFormRow">
			<div class="wFormInput">
				<input class="wInput" required="" type="text" data-name="name" name="name" placeholder="<?php echo __('Имя');?>" data-rule-minlength="2" data-rule-word="true" aria-required="true">
				<div class="inpInfo"><?php echo __('Имя');?></div>
			</div>
		</div>

		<div class="wFormRow">
			<div class="wFormInput">
				<input class="wInput phone-mask" required="required" data-name="phone" type="tel" name="phone" placeholder="<?php echo __('Телефон');?>" data-rule-phone="true" data-inputmask="'alias': 'phone'"/>
				<div class="inpInfo"><?php echo __('Телефон');?></div>
			</div>
		</div>

		<div class="wFormRow">
			<div class="wFormInput">
				<textarea class="wTextarea" name="msg" data-name="text" placeholder="<?php echo __('Комментарий');?>" data-rule-minlength="10"></textarea>
				<div class="inpInfo"><?php echo __('Комментарий');?></div>
			</div>
		</div>
		<input type="hidden" data-name="lang" value="<?php echo \I18n::lang();?>">
		<?php if(array_key_exists('token', $_SESSION)): ?>
			<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
		<?php endif; ?>
		<div class="wFormRow w_last m-tac">
			<button class="wSubmit button"><?php echo __('Подтверждение');?></button>
		</div>
	</div>
</div>*/?>



<div class="mfiModal mfiModal--form">
	<button title="Закрыть (ESC)" type="button" class="mfp-close">&times;</button>
	<div class="pageTitle">Заказать<br>обратный звонок</div>
	<div class="wTxt">
		<p>Наш менеджер свяжется с вами в течении 5 минут</p>
	</div>
	<div class="wForm wFormDef" data-form="true" data-ajax="callback">
		<div class="wFormRowParter _clear-after">
			<div class="wFormRow">
				<small>Ваше номер<span class="_color-danger">*</span></small>
				<div class="wFormInput _with-arrow">
					<input class="wInput js-inputmask" type="text" data-name="phone" name="callback-user-phone" id="callback-user-phone" required data-rule-pattern="\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}" data-msg-pattern="Укажите корректный номер" placeholder="+7 (___) ___-__-__">
				</div>
			</div>
			<div class="wFormRow">
				<small>Время звонка<span class="_color-danger">*</span></small>
				<div class="wFormInput">
					<select class="wInput" name="callback-call-time" data-name="time" id="callback-call-time">
						<option value="8:00">8:00</option>
						<option value="9:00">9:00</option>
						<option value="10:00" selected>10:00</option>
						<option value="11:00">11:00</option>
						<option value="12:00">12:00</option>
						<option value="13:00">13:00</option>
						<option value="14:00">14:00</option>
						<option value="15:00">15:00</option>
						<option value="16:00">16:00</option>
						<option value="17:00">17:00</option>
						<option value="18:00">18:00</option>
						<option value="19:00">19:00</option>
						<option value="20:00">20:00</option>
						<option value="21:00">21:00</option>
						<option value="22:00">22:00</option>
					</select>
				</div>
			</div>
		</div>
		<?php if(array_key_exists('token', $_SESSION)): ?>
			<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
		<?php endif; ?>
		<div class="wFormRow" style="max-width: 310px;">
			<button class="button button--primary button--noround button--expand wInputwSubmit wSubmit"><span>Заказать звонок</span></button>
		</div>
	</div>
</div>
