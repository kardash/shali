<div class="contact-form">
	<div class="h1 h1--left"><?php echo __('отправить сообщение');?></div>
	<div data-form="true" data-ajax="contacts" class="wForm wFormDef" novalidate="novalidate">
		<div class="contact-form_row clearfix">
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required type="text" data-name="name" name="mane" placeholder="<?php echo __('Введите Ваше имя');?>" data-rule-minlength="2" data-rule-word="true" aria-required="true">
					<div class="inpInfo"><?php echo __('Ваше имя');?></div>
				</div>
			</div>
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required type="email" data-name="email" name="mail" placeholder="<?php echo __('Введите Ваш e-mail');?>" data-rule-email="true" aria-required="true">
					<div class="inpInfo"><?php echo __('Ваш e-mail');?></div>
				</div>
			</div>
		</div>
		<div class="wFormRow">
			<div class="wFormInput">
				<textarea class="wTextarea" required name="msg"  data-name="text" placeholder="<?php echo __('Текст сообщения');?>" data-rule-minlength="10" aria-required="true"></textarea>
				<div class="inpInfo"><?php echo __('Текст сообщения');?></div>
			</div>
		</div>
		<input type="hidden" data-name="lang" value="<?php echo \I18n::lang();?>">
		<?php if(array_key_exists('token', $_SESSION)): ?>
			<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
		<?php endif; ?>
		<div class="wFormRow w_last">
			<button class="wSubmit button"><?php echo __('отправить');?></button>
		</div>
	</div>
</div>